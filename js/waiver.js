/**
 * @author stu
 */
$(document).ready(function() {
	window.file = sessionStorage.getItem("pdf");

	$(document.body).on('click', '#requestWaiver', function(event) {
		//get unit codes, if they were not selected return null
		unit1Code = ($('#unit1code').text() != '') ? $('#unit1code').text() : 'null';
		unit2Code = ($('#unit2code').text() != '') ? $('#unit2code').text() : 'null';
		unit3Code = ($('#unit3code').text() != '') ? $('#unit3code').text() : 'null';
		studentName = sessionStorage.getItem("studentName");
		studentID = sessionStorage.getItem("studentID");
		
		//retieve emails for each unit code using getEmail function which takes reelevant unit code as its parameter
		var noUnits = 0;

		getEmails(unit1Code, unit2Code, unit3Code);

		event.preventDefault();
	});

	//function to match unit code to staff id and then use staff id to retrieve relevant email
	function getEmails(unit1, unit2, unit3) {
		//initialise emails to null, 1 for unit 1 etc, used to choose which to send
		url = 'getEmail.php';
		email1 = "null";
		email2 = "null";
		email3 = "null";

		//these unit codes are passed to the php url using get, it will be encoded with JSON
		unitCodes = {
			unit1Code : unit1,
			unit2Code : unit2,
			unit3Code : unit3,
			pdf : window.file
		};
		//returns JSON encoded encoded array which will contain max one row of array hence row [0] is only used
		$.getJSON(url, unitCodes, function(data) {
			processEmail(data[0].email1, data[0].email2, data[0].email3);
		});

		function processEmail(email1, email2, email3) {
			if (email1 != "null")
				sendEmail(email1, unit1Code);
			if (email2 != "null")
				sendEmail(email2, unit2Code);
			if (email3 != "null")
				sendEmail(email3, unit3Code);
		}

	};

	function sendEmail(emailAddress, unit) {
		//data to send using POST function
		var data = {
			address : emailAddress,
			unitCode : unit,
			name : studentName,
			id : studentID,
			pdf : window.file
		};

		$.post('sendEmail.php', data, function(result) {
			if (result == "success") {
				//show popup
				$('.popUpWrapper').fadeIn();
				$.post("deleteFile.php", {
					thisName : window.file
				}, function(result) {

				});

				sessionStorage.clear();
			} else if (result == "error") {
				alert("Request Not Sent, please contact course planning directly.");
				$.post("deleteFile.php", {
					thisName : window.file
				}, function(result) {

				});
				sessionStorage.clear();
			}
		});

	}


	$(document.body).on('click', '#waiverPopUpContinueButton', function(event) {
		//hide popup
		$('.popUpWrapper').hide();
		event.preventDefault();
		$.post("deleteFile.php", {
			thisName : window.file
		}, function(result) {

		});
		window.location.href = "index.html";
	});

	$(document.body).on('click', '#goBackButton', function(event) {
		//show popup
		event.preventDefault();
		window.location.href = "waiverUnitSelect.php";
	});
});
