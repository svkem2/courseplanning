$(document).ready(function() {
	
	//Save to pdf button clicked
	$(document.body).on('click', '#requestChecklist', function(event){
		var planner = $("#plannerForm").html();
		$.post("sendAsPDF.php", {
				planner : planner
			}, function(result) {
				if(result == 'success'){
					$('.popUpWrapper').fadeIn();
				}
				else {
					alert("System Error. Please contact course planning directly.");
				}
				
			});
		event.preventDefault();
	});
	
	
	
	$(document.body).on('click', '#goBackButton', function(event){
		//show popup
		event.preventDefault();
		window.location.href = "index.html";
	});	
	
	$(document.body).on('click', '#popUpContinueButton', function(event){
		//show popup
		event.preventDefault();
		//hide popup
		$('.popUpWrapper').hide();
		window.location.href = "index.html";
	});
	
		$(document.body).on('click', '#bookAppointmentButton', function(event){
		//show popup
		event.preventDefault();
		window.open('http://goo.gl/X2lqM', '_blank');
	});	
});