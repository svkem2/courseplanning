/**
 * @author stu
 */
$(document).ready(function() {
	window.completedUnits = [];
	window.completedUnits = JSON.parse(sessionStorage.getItem("completedUnits"));
	units = new Array();
	function initDropdowns(type) {
		//get completed units array

		$.getJSON("grabUnitsWaiver.php", function(data) {
			html = '<option value="Please Select Unit">Please Select Unit</option>';
			units = [];
			$.each(data, function(index, data) {
				completed = false;
				i = 0;
				for ( i = 0; i < window.completedUnits.length; ++i) {
					if (data.unitCode == window.completedUnits[i]) {
						completed = true;
					}
				}

				if (!completed) {
					html += '<option value="' + data.unitCode + '">' + data.unitCode + '</option>';
					units.push(data.unitCode);
				}

			});

			if (type == "init") {

				$("#unit1").html(html);
				$("#unit2").html(html);
				$("#unit3").html(html);
			}

		});
	}

	initDropdowns("init");

	unit1 = "Please Select Unit";
	unit2 = "Please Select Unit";
	unit3 = "Please Select Unit";
	numOfUnits = "0";

	/* variables used to hold unit selection choices */

	/**
	 * Select number of units for prerequisite waiver:
	 * Wait for change event on numChosen Select
	 * Get value from numChosen Select
	 * Use value to display appropriate number of unit comboboxes
	 */

	$("#numChosen").change(function() {
		numOfUnits = $(this).val();
		switch(numOfUnits) {
		case "1":
			$("#unit1Row").fadeIn();
			$("#unit2Row").hide();
			$("#unit3Row").hide();
			HTML = "<option value='1' selected>1</option><option value='2'>2</option><option value='3'>3</option>";
			$("#numChosen").html(HTML);
			break;
		case "2":
			$("#unit1Row").fadeIn();
			$("#unit2Row").fadeIn();
			$("#unit3Row").hide();
			HTML = "<option value='1'>1</option><option value='2' selected>2</option><option value='3'>3</option>";
			$("#numChosen").html(HTML);
			break;
		case "3":
			$("#unit1Row").fadeIn();
			$("#unit2Row").fadeIn();
			$("#unit3Row").fadeIn();
			HTML = "<option value='1'>1</option><option value='2'>2</option><option value='3' selected>3</option>";
			$("#numChosen").html(HTML);
			break;
		}
	});

	HTML = "";
	semaphoreListProcessing = "false";

	initDropdowns("noFill");
	$.getJSON("grabUnits.php", function(data) {

		var i = 0;
		while (i < data.myarray.length) {
			if (jQuery.inArray(data.myarray[i], window.completedUnits) != -1) {
				units[i] = data.myarray[i];
			}
			i++;
		}

	});

	function populateDropdowns(unitBoxNumber, selectedUnit) {
		semaphoreListProcessing = "true";

		switch(unitBoxNumber) {
		case "1" :
			processList("#unit2", unit2, selectedUnit);
			processList("#unit3", unit3, selectedUnit);

			break;
		case "2" :
			processList("#unit1", unit1, selectedUnit);
			processList("#unit3", unit3, selectedUnit);
			break;
		case "3" :
			processList("#unit1", unit1, selectedUnit);
			processList("#unit2", unit2, selectedUnit);
			break;

		}

		semaphoreListProcessing = "false";

	}

	function processList(list, currentSelection, unitToRemove) {
		HTML = '<option value="Please Select Unit">Please Select Unit</option>';
		switch(list) {//set other selected units to other 1 and other 2
		case "#unit1":
			other1 = unit2;
			other2 = unit3;
			break;
		case "#unit2":
			other1 = unit1;
			other2 = unit3;
			break;
		case "#unit3":
			other1 = unit1;
			other2 = unit2;
			break;
		}

		$.each(units, function(index, value) {
			if (value != unitToRemove && value != other1 && value != other2) {//make sure already slecetd unit does not get added to other lists
				if (value == currentSelection)
					HTML += '<option value="' + value + '" selected>' + value + '</option>';
				else
					HTML += '<option value="' + value + '">' + value + '</option>';
			}
		});
		$(list).html(HTML);
	}

	/**
	 * When value is changed in unit dropdown list
	 * Store values into global variables
	 */

	$(document.body).on('click', '#unit1', function(e) {
		e.preventDefault();
		if (semaphoreListProcessing == "false") {
			unit1 = $(this).val();
			populateDropdowns("1", unit1);
		}
	});

	$(document.body).on('click', '#unit2', function(e) {
		e.preventDefault();
		if (semaphoreListProcessing == "false") {
			unit2 = $(this).val();
			populateDropdowns("2", unit2);
		}
	});

	$(document.body).on('click', '#unit3', function(e) {
		e.preventDefault();
		if (semaphoreListProcessing == "false") {
			unit3 = $(this).val();
			populateDropdowns("3", unit3);
		}
	});

	/**
	 * PROCESS SELECTION:
	 * Validate that selections have been made
	 * Store selections to local storage
	 * Used sessionStorage for local storage as no need for 'long-term' storage
	 */

	$("#proceed").click(function() {

		var isSelected = false;
		switch(numOfUnits) {/*check if selection is made */
		case "1":
			if (unit1 != 'Please Select Unit' && unit1 != 'none')
				isSelected = true;
			break;
		case "2":
			if (unit1 != 'Please Select Unit' && unit1 != 'none' && unit2 != 'Please Select Unit' && unit2 != 'none' && unit1 != unit2)
				isSelected = true;
			break;
		case "3":
			if (unit1 != 'Please Select Unit' && unit1 != 'none' && unit2 != 'Please Select Unit' && unit2 != 'none' && unit3 != 'Please Select Unit' && unit3 != 'none' && unit1 != unit2 && unit1 != unit3 && unit2 != unit3)
				isSelected = true;
			break;
		default:
			isSelected = false;
			break;
		}

		// Send Ajax request to backend.php, with src set as "img" in the POST data

		if (isSelected != false) {
			//store selections in PHP Session Variables
			$.post("waiverBackend.php", {
				'unit1' : unit1,
				'unit2' : unit2,
				'unit3' : unit3
			});

			if ( typeof (Storage) !== "undefined") {
				sessionStorage.setItem("unit1Selection", unit1);
				sessionStorage.setItem("unit2Selection", unit2);
				sessionStorage.setItem("unit3Selection", unit3);

			} else {
				alert("Please do not use Internet Explorer, for your own sanity...");
			}
			window.location.href = "waiver.php";
		} else {
			if (isSelected == false)
				alert("Please select unit/s");
		}

	});

	$(document.body).on('click', '#goBackButton', function(event) {
		//show popup
		event.preventDefault();
		window.location.href = "index.html";
	});

});
