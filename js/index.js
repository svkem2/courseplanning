$(document).ready(function() {
	sessionStorage.clear();
	//maintain tmp folder - delete files older than 
	$.post("cleanTemp.php", function(result) {
		});
	
	//hide javascript disabled popup
	$('.popUpWrapper').hide();
	//Save to pdf button clicked
	$(document.body).on('click', '#coursePlanningButton', function(event){
		//show popup
		event.preventDefault();
		sessionStorage.setItem("dest", "planner.html");
		window.location.href = "studentUpload.html";
	});
	
//Save to pdf button clicked
	$(document.body).on('click', '#waiverButton', function(event){
		//show popup
		event.preventDefault();
		sessionStorage.setItem("dest", "plannerForWaiver.html");
		window.location.href = "studentUpload.html";
	});
	
});