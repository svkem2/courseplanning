$(document).ready(function() {
	//get sessionStorage variables
	window.seed = sessionStorage.getItem("seed");
	window.thisName = window.seed + ".html";
	window.pdf = window.seed + ".pdf";

	//add student details from scrapped data
	$.getJSON("scrape/scrapeStudentDetails.py", {
		"thisName" : thisName
	}, function(data) {
		//get student details html  into variables
		studentName = "<p>" + data[0].studentName + "</p>";
		studentID = "<p>" + data[0].studentNumber + "</p>";

		sessionStorage.setItem("studentName", data[0].studentName);
		sessionStorage.setItem("studentID", data[0].studentNumber);

		//add html to required fields
		$("#studentName").html(studentName);
		$("#studentID").html(studentID);

	});

	//add course information from scrapped data
	$.getJSON("scrape/scrapeCourseInfo.py", {
		"thisName" : thisName
	}, function(data) {

		$.each(data, function(index, data) {
			courseTitle = "<p>" + data.courseTitle + "</p>";
			WAM = "<p>" + data.WAM + "</p>";
			GPA = "<p>" + data.GPA + "</p>";

			newRow = '<tr><td>' + courseTitle + '</td>';
			newRow += '<td>' + GPA + '</td>';
			newRow += '<td>' + WAM + '</td></tr>';

			$("#courseInformation").append(newRow);

		});

	});

	//create array to hold majors
	majors = [];
	//append required major units to html page
	addRequiredUnits();
	function addRequiredUnits() {

		$.ajax({
			url : "getMajors.php",
			dataType : 'json',
			async : false,
			success : function(data) {
				$.each(data, function(index, data) {
					majors.push(data.majorCode);
					html = '<br/><h2>&nbsp; Units Required for ' + data.majorName + '</h2><br/>';
					html += '<table id ="' + data.majorCode + '" class="tableLayout">';
					html += processRequiredUnits(data.majorCode);
					html += '</table>';

					$("#requiredUnits").append(html);

				});

			}
		});

	}

	//function to fill array with required units, and fill requiredUnits HTML tables
	function processRequiredUnits(major) {

		newRows = '<tr><td class="header"><p><strong>Unit</strong></p></td><td class="header"><p><strong>Completed</strong></p></td>';
		newRows += '<td class="header"><p><strong>Unit</strong></p></td><td class="header"><p><strong>Completed</strong></p></td></tr>';
		firstColoumnCompleted = false;
		url = "getRequiredUnits.php";

		$.ajax({
			url : url,
			dataType : 'json',
			async : false,
			data : {
				majorCode : major
			},
			success : function(data) {
				newRow = "";
				$.each(data, function(index, data) {

					//create new row to append from fetched data
					if (!firstColoumnCompleted) {
						newRow += '<tr><td id="req' + major + data.unit + '"><p>' + data.unit + '</p></td>';
						newRow += '<td id="' + major + data.unit + '" class="orange"><p>No</p></td>';
						firstColoumnCompleted = true;
					} else {
						newRow += '<td id="req' + major + data.unit + '"><p>' + data.unit + '</p></td>';
						newRow += '<td id="' + major + data.unit + '" class="orange"><p>No</p></td></tr>';
						firstColoumnCompleted = false;
					}

					if (!firstColoumnCompleted) {
						newRows += newRow;
						newRow = "";
					}

				});
				//add odd coloumn
				if (firstColoumnCompleted) {
					newRows += "</tr>";
				}

			}
		});

		return newRows;
	}
	
	//database to hold units which have receieved full point or passed, used for prerequisite selections
	window.completedUnits = [];

	//append unit records from scrapped records
	$.getJSON("scrape/scrapeRecords.py", {
		thisName : thisName
	}, function(data) {
		//get first records year and semester and use to create first row of table
		//these values will also be checked and updated if new year semester combo is found
		currentYear = data[0].year;
		currentSemester = data[0].period;

		//create variables for levels counters
		level1Count = 0;
		level1FITCount = 0;
		level1Points = 0;
		level1FITPoints = 0;
		level2Count = 0;
		level2FITCount = 0;
		level2Points = 0;
		level2FITPoints = 0;
		level3Count = 0;
		level3FITCount = 0;
		level3Points = 0;
		level3FITPoints = 0;
		totalLevels = 0;
		totalLevelsFIT = 0;
		totalPoints = 0;
		totalPointsFIT = 0;

		//create initial table row (tr) by appending a row to table with id=courseChecklist
		newRow = '<tr id="' + currentYear + currentSemester + '">';
		newRow += '<td class="header"><h3>' + currentYear + '</h3></td>';
		newRow += '<td><h3>' + currentSemester + '</h3></td>';
		newRow += '</tr>';

		$("#courseChecklist").append(newRow);

		$.each(data, function(index, data) {
			//get all required unit details into variables
			unitYear = data.year;
			unitSemester = data.period;
			unitCode = data.unitCode;
			unitGrade = data.grade;
			unitOther = data.other;
			//will be 'attempted' if exam written otherwise exempted, withdrawn, discontinued etc
			unitPoints = data.points;
			unitTitle = data.unitTitle;
			unitMark = data.mark;
			unitLevel = data.unitLevel;

			//adjust unit level counters
			if ((unitLevel == "1") && (unitGrade == "P" || unitGrade == "C" || unitGrade == "D" || unitGrade == "HD" || unitGrade == "NE" || unitOther == "EXEMPTED")) {
				window.completedUnits.push(unitCode);
				level1Count++;
				level1Points += 6;
				totalLevels++;
				totalPoints += 6;
				if (unitCode.substring(0, 3) == "FIT") {
					level1FITCount++;
					level1FITPoints += 6;
					totalLevelsFIT++;
					totalPointsFIT += 6;

				}
			} else if ((unitLevel == "2") && (unitGrade == "P" || unitGrade == "C" || unitGrade == "D" || unitGrade == "HD" || unitGrade == "NE" || unitOther == "EXEMPTED")) {
				window.completedUnits.push(unitCode);
				level2Count++;
				level2Points += 6;
				totalLevels++;
				totalPoints += 6;
				if (unitCode.substring(0, 3) == "FIT") {
					level2FITCount++;
					level2FITPoints += 6;
					totalLevelsFIT++;
					totalPointsFIT += 6;
				}
			} else if ((unitLevel == "3") && (unitGrade == "P" || unitGrade == "C" || unitGrade == "D" || unitGrade == "HD" || unitGrade == "NE" || unitOther == "EXEMPTED")) {
				window.completedUnits.push(unitCode);
				level3Count++;
				level3Points += 6;
				totalLevels++;
				totalPoints += 6;
				if (unitCode.substring(0, 3) == "FIT") {
					level3FITCount++;
					level3FITPoints += 6;
					totalLevelsFIT++;
					totalPointsFIT += 6;
				}
			}

			var i;
			for ( i = 0; i < majors.length; ++i) {
				unitsArray = [];
				$.ajax({
					url : "getMajorUnits.php",
					dataType : 'json',
					async : false,
					data : {
						majorCode : majors[i]
					},
					success : function(data) {

						unitsArray = [];

						$.each(data, function(index, data) {
							unitsArray.push(data.majorRequiredUnit);
						});
						
						if ((jQuery.inArray(unitCode, unitsArray) != -1) && (unitGrade == "P" || unitGrade == "C" || unitGrade == "D" || unitGrade == "HD" || unitGrade == "NE" || unitOther == "EXEMPTED")) {
							$("#" + majors[i] + unitCode).html("<p>Yes</p>");
							$("#" + majors[i] + unitCode).css("background-color", "#C2FFEB");
						}

						if (data == "error") {
							alert("System Fault. Pleas contact administrator");
						}
					}
				});

			}

			//check if new row needs to be added else append to current row
			if ((currentYear + currentSemester) == (unitYear + unitSemester)) {
				appendUnit();
			} else {
				createNewRow();
				appendUnit();
			}

			function appendUnit() {
				//check if this unit has been attempted then add normally otherwise add other type of entry e.g. DISCONTINUED
				if (unitOther == "attempted") {
					newData = '<td id="' + unitYear + unitSemester + unitCode + '">';
					newData += '<p>' + unitCode + ' (' + unitGrade + ')</p>';
				} else {
					newData = '<td id="' + unitYear + unitSemester + unitCode + '">';
					newData += '<p>' + unitCode + ' (' + unitOther + ')</p>';
				}

				//append to current row
				$("#" + currentYear + currentSemester).append(newData);

				//set correct background color to table data
				if (unitGrade == "N" || unitGrade == "WN" || unitGrade == "WDN" || unitGrade == "WI") {
					$("#" + unitYear + unitSemester + unitCode).css("background-color", "#FF6600");
				} else if (unitGrade == "P" || unitGrade == "C" || unitGrade == "D" || unitGrade == "HD" || unitGrade == "NE" || unitOther == "INCOMPLETE" || unitOther == "EXEMPTED") {
					$("#" + unitYear + unitSemester + unitCode).css("background-color", "#C2FFEB");
				} else {
					$("#" + unitYear + unitSemester + unitCode).css("background-color", "#94B8FF");
				}
			}

			function createNewRow() {
				//change to new year/semester combo
				currentYear = unitYear;
				currentSemester = unitSemester;

				//add new row to courseChecklist
				newRow = '<tr id="' + currentYear + currentSemester + '">';
				newRow += '<td class="header"><h3>' + currentYear + '</h3></td>';
				newRow += '<td><h3>' + currentSemester + '</h3></td>';
				newRow += '</tr>';

				$("#courseChecklist").append(newRow);

			}
			
			

		});
		//add level counts to planner
		level1Count = "<p>" + level1Count + "</p>";
		level1FITCount = "<p>" + level1FITCount + "</p>";
		level2Count = "<p>" + level2Count + "</p>";
		level2FITCount = "<p>" + level2FITCount + "</p>";
		level3Count = "<p>" + level3Count + "</p>";
		level3FITCount = "<p>" + level3FITCount + "</p>";
		totalLevels = "<p>" + totalLevels + "</p>";
		totalLevelsFIT = "<p>" + totalLevelsFIT + "</p>";

		$("#level1UnitsCompleted").html(level1Count);
		$("#level1FITUnitsCompleted").html(level1FITCount);
		$("#level2UnitsCompleted").html(level2Count);
		$("#level2FITUnitsCompleted").html(level2FITCount);
		$("#level3UnitsCompleted").html(level3Count);
		$("#level3FITUnitsCompleted").html(level3FITCount);
		$("#levelAllUnitsCompleted").html(totalLevels);
		$("#levelAllUnitsFITCompleted").html(totalLevelsFIT);

		//add points counts to planner
		level1PointsHTML = "<p>" + level1Points + "</p>";
		level1FITPointsHTML = "<p>" + level1FITPoints + "</p>";
		level2PointsHTML = "<p>" + level2Points + "</p>";
		level2FITPointsHTML = "<p>" + level2FITPoints + "</p>";
		level3PointsHTML = "<p>" + level3Points + "</p>";
		level3FITPointsHTML = "<p>" + level3FITPoints + "</p>";
		totalPointsHTML = "<p>" + totalPoints + "</p>";
		totalPointsFITHTML = "<p>" + totalPointsFIT + "</p>";

		$("#level1PointsCompleted").html(level1PointsHTML);
		$("#level1FITPointsCompleted").html(level1FITPointsHTML);
		$("#level2PointsCompleted").html(level2PointsHTML);
		$("#level2FITPointsCompleted").html(level2FITPointsHTML);
		$("#level3PointsCompleted").html(level3PointsHTML);
		$("#level3FITPointsCompleted").html(level3FITPointsHTML);
		$("#levelAllPointsCompleted").html(totalPointsHTML);
		$("#levelAllPointsFITCompleted").html(totalPointsFITHTML);

		level1Rule = 0;
		level2Rule = 0;
		level3Rule = 0;
		levelAllRule = 0;

		$.getJSON("grabRules.php", function(data) {

			$.each(data, function(index, data) {
				if (data.ruleName == "level1Rule")
					level1Rule = data.ruleValue;
				if (data.ruleName == "level2Rule")
					level2Rule = data.ruleValue;
				if (data.ruleName == "level3Rule")
					level3Rule = data.ruleValue;
				if (data.ruleName == "levelAllRule")
					levelAllRule = data.ruleValue;
				if (data == "error")
					alert("System error, Please contact course planning directly");

			});

			//enter rules
			$("#level1Rule p").text("<= " + level1Rule);
			$("#level2Rule p").text("<= " + level2Rule);
			$("#level3Rule p").text("<= " + level3Rule);
			$("#levelAllRule p").text("<= " + levelAllRule);

			//adjust rule colour coding
			if (level1Points <= level1Rule)
				$("#level1Rule").css("background-color", "#C2FFEB");
			else
				$("#level1Rule").css("background-color", "#FF6600");
			if (level2Points >= level2Rule)
				$("#level2Rule").css("background-color", "#C2FFEB");
			else
				$("#level2Rule").css("background-color", "#FF6600");
			if (level3Points >= level3Rule)
				$("#level3Rule").css("background-color", "#C2FFEB");
			else
				$("#level3Rule").css("background-color", "#FF6600");
			if (totalPoints >= levelAllRule)
				$("#levelAllRule").css("background-color", "#C2FFEB");
			else
				$("#levelAllRule").css("background-color", "#FF6600");

		});
		
		sessionStorage.setItem("completedUnits", JSON.stringify(window.completedUnits));

	});
	//Save to pdf button clicked
	$(document.body).on('click', '#requestChecklist', function(event) {
		var planner = $("#plannerForm").html();
		var file = window.seed;
		var pathFile = "../tmp/" + window.pdf;
		$.post("savePDF.php", {
			planner : planner,
			file : file
		}, function(result) {
			if (result == 'success') {
				sessionStorage.setItem("pdf", window.pdf);
				$.post("deleteFile.php", {
					thisName : window.thisName
				}, function(result) {

				});
				window.location.href = "waiverUnitSelect.php";
			} else {
				alert("System Error. Please contact course planning directly.");
			}

		});
		event.preventDefault();
	});

	$(document.body).on('click', '#goBackButton', function(event) {
		//show popup
		event.preventDefault();
		$.post("deleteFile.php", {
			thisName : window.thisName
		}, function(result) {

		});

		$.post("deleteFile.php", {
			thisName : window.pdf
		}, function(result) {

		});
		window.location.href = "index.html";
	});

	$(document.body).on('click', '#popUpContinueButton', function(event) {
		//show popup
		event.preventDefault();
		//hide popup
		$('.popUpWrapper').hide();
		$.post("deleteFile.php", {
			thisName : window.thisName
		}, function(result) {

		});

		$.post("deleteFile.php", {
			thisName : window.pdf
		}, function(result) {

		});
		sessionStorage.clear();
		window.location.href = "index.html";

	});

	$(document.body).on('click', '#bookAppointmentButton', function(event) {
		//show popup
		event.preventDefault();
		window.open('http://goo.gl/X2lqM', '_blank');
	});
	
	

});

