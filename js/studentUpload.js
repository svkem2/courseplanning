$(document).ready(function() {
	$(".popUpWrapper").hide();
	
	//get destination after this upload
	window.destination = sessionStorage.getItem("dest");
	
	//create random number to store file as
	window.seed = Math.floor(Math.random() * 1E16);
	window.filename = window.seed + ".html";
	function sendFileToServer(formData, status) {
		/*************/
		var uploadURL = "tempupload.php";
		//we need to add upload url
		formData.append('seed', window.filename);
		//Extra Data.
		var jqXHR = $.ajax({
			xhr : function() {
				var xhrobj = $.ajaxSettings.xhr();
				if (xhrobj.upload) {
					xhrobj.upload.addEventListener('progress', function(event) {
						var percent = 0;
						var position = event.loaded || event.position;
						var total = event.total;
						if (event.lengthComputable) {
							percent = Math.ceil(position / total * 100);
						}
						//Set progress
						status.setProgress(percent);
					}, false);
				}
				return xhrobj;
			},
			url : uploadURL,
			type : "POST",
			contentType : false,
			processData : false,
			cache : false,
			data : formData,
			success : function(data) {
				if (data == "error") {
					inValidFile();
					status.setProgress(0);
				} else if (data == "success") {
					status.setProgress(100);
					successHTML = '<li><p>Record uploaded successfully!</p></li>';
					successHTML += '<li><p>Please click Proceed to continue</p></li>';
					$(".infoPanel ul").html(successHTML);
					$("#dragandrophandler").hide();
					
					//pass on name of file to be processed
					sessionStorage.setItem("seed", window.seed);

					window.location.href = window.destination;

				} else {
					alert("System Fault. Please contact Course Planning Directly");
				}
			}
		});

		status.setAbort(jqXHR);
	}

	//if file invalid
	function inValidFile() {
		message = "<p>Please ensure your WES file is correctly saved as an html file</p>";
		message += "<p>For more information follow the links below depending on which browser you use</p>";
		message += '<ul><li><a href="https://support.mozilla.org/en-US/kb/how-save-web-page" target="_blank">FireFox</a></li>';
		message += '<li><a href="https://chrome.google.com/webstore/detail/singlefile/mpiodijhokgodhhofbcjdecpffjipkle?hl=en" target="_blank">Google Chrome</a></li>';
		message += '<li><a href="http://windows.microsoft.com/en-za/windows-vista/save-a-webpage-as-a-file" target="_blank">Internet Explorer</a></li>';
		message += '<li><a href="http://browsers.about.com/od/op3/ss/save-web-pages-opera-11_2.htm#step-heading" target="_blank">Opera</a></li>';
		message += '<li><a href="http://support.apple.com/kb/PH11875" target="_blank">Apple Safari</a></li></ul>';

		showPopUp("Incorrect filetype", message, "index.html", "Home", "studentUpload.html", "Try Again");
	}

	//takes variables builds and displays popup
	function showPopUp(header, message, button1Href, button1Msg, button2Href, button2Msg) {
		headerHTML = '<h1>' + header + '</h1>';
		messageHTML = message;
		button1HTML = '<a href="' + button1Href + '"><p>' + button1Msg + '</p></a>';
		button2HTML = '<a href="' + button2Href + '"><p>' + button2Msg + '</p></a>';

		// add message
		$(".popUpHeader").html(headerHTML);

		$('#popUpText').html(messageHTML);
		// add button 1
		$("#button1").html(button1HTML);
		//add button 2
		$("#button2").html(button2HTML);
		//display popup
		$(".popUpWrapper").fadeIn();

	}

	var rowCount = 0;
	function createStatusbar(obj) {
		rowCount++;
		var row = "odd";
		if (rowCount % 2 == 0)
			row = "even";
		this.statusbar = $("<div class='statusbar " + row + "'></div>");
		this.filename = $("<div class='filename'></div>").appendTo(this.statusbar);
		this.size = $("<div class='filesize'></div>").appendTo(this.statusbar);
		this.progressBar = $("<div class='progressBar'><div></div></div>").appendTo(this.statusbar);
		this.abort = $("<div class='abort'>Cancel</div>").appendTo(this.statusbar);
		obj.after(this.statusbar);

		this.setFileNameSize = function(name, size) {
			var sizeStr = "";
			var sizeKB = size / 1024;
			if (parseInt(sizeKB) > 1024) {
				var sizeMB = sizeKB / 1024;
				sizeStr = sizeMB.toFixed(2) + " MB";
			} else {
				sizeStr = sizeKB.toFixed(2) + " KB";
			}

			this.filename.html(name);
			this.size.html(sizeStr);
		};
		this.setProgress = function(progress) {
			var progressBarWidth = progress * this.progressBar.width() / 100;
			this.progressBar.find('div').animate({
				width : progressBarWidth
			}, 10).html(progress + "% ");
			if (parseInt(progress) >= 100) {
				this.abort.hide();
			}
		};
		this.setAbort = function(jqxhr) {
			var sb = this.statusbar;
			this.abort.click(function() {
				jqxhr.abort();
				sb.hide();
			});
		};
	}

	function handleFileUpload(files, obj) {
		for (var i = 0; i < files.length; i++) {
			var fd = new FormData();
			fd.append('file', files[i]);

			var status = new createStatusbar(obj);
			//Using this we can set progress.
			status.setFileNameSize(files[i].name, files[i].size);
			sendFileToServer(fd, status);

		}
	}


	$(document).ready(function() {
		var obj = $("#dragandrophandler");
		obj.on('dragenter', function(e) {
			e.stopPropagation();
			e.preventDefault();
			$(this).css('border', '2px solid #124c8a');
		});
		obj.on('dragover', function(e) {
			e.stopPropagation();
			e.preventDefault();
		});
		obj.on('drop', function(e) {

			$(this).css('border', '2px dotted #124c8a');
			e.preventDefault();
			var files = e.originalEvent.dataTransfer.files;

			//We need to send dropped files to Server
			handleFileUpload(files, obj);
		});
		$(document).on('dragenter', function(e) {
			e.stopPropagation();
			e.preventDefault();
		});
		$(document).on('dragover', function(e) {
			e.stopPropagation();
			e.preventDefault();
			obj.css('border', '2px dotted #124c8a');
		});
		$(document).on('drop', function(e) {
			e.stopPropagation();
			e.preventDefault();
		});

	});
	//Save to pdf button clicked
	$(document.body).on('click', '#plannerButton', function(event) {
		//

		//show popup
		event.preventDefault();

		window.location.href = "planner.html";

	});

	$(document.body).on('click', '#goBackButton', function(event) {
		//show popup
		event.preventDefault();

		//delete file and session storage of filename
		$.post("deleteFile.php", {
			thisName : window.filename
		}, function(result) {

		});
		sessionStorage.clear();
		window.location.href = "index.html";
	});
});
