$(document).ready(function() {

	$('#scrape').click(function(e) {
		e.preventDefault();

		//Extract Full Name and Surname from WES iFrame
		getRawName();
	});

	//************************ Funtions:

	//GET NAME retrieve full name from WES and store in global variable fullNameRaw - raw because it is unformatted

	function getRawName() {
		var fullNameRaw, wes;
		
		wes = $('#wesIFrame').contents();
		
		alert(wes);

		fullNameRaw = $('p').filter(function() {
			return $(this).text() === "<strong>Monash University Unofficial Student Academic Record</strong>";
		}).next().text();
		
		alert(fullNameRaw);

	}

});
