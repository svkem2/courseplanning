<?php

//get variables from POST
$toAddress = $_POST['address'];
$unitCode = $_POST['unitCode'];
$studentName = $_POST['name'];
$studentID = $_POST['id'];
$file = $_POST['pdf'];

//temp to address for testing
//$tempTo = "svkem2@student.monash.edu";

//create other mail() variables
$subject = "Prerequisite Waiver Request: Unit: ". $unitCode .", Student ID: ". $studentID . ".";
$message = "<p>To Whom It May Concern,<p><br />";
$message .= "<b>Automated Prerequisite Waiver Request</b><br />";
$message .= "<p><b>Unit: </b>". $unitCode ."</p>";
$message .= "<p><b>Student Name: </b>". $studentName ."</p>";
$message .= "<p><b>Student ID: </b>". $studentID ."</p>";
$message .= "<p><b>Please find the attached student record.</b></p><br />";
$message .= "<p>Thank you and have a good day!</p>";
$message .= "<p>Monash Faculty of IT, Course Planning.</p>";

   //send email
require_once (dirname(__FILE__) . '/mailer/class.phpmailer.php');
$email = new PHPMailer();
$email -> From = 'neil.manson@monash.edu';
$email -> FromName = 'Neil Manson';
$email -> Subject = $subject;
$email -> Body = $message;
$email -> AddAddress($toAddress);
//$email -> addCC('neil.manson@monash.edu');

$email -> AddAttachment('/var/www/html/tmp/'.$file, $studentID.".pdf");
$email -> isHTML(true);
if (!$email -> send()) {
	echo 'emailError';

} else {
	echo 'success';
}

?>