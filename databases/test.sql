CREATE TABLE IF NOT EXISTS `majorRequiredUnits` (
  `majorRequiredUnitsID` int(11) NOT NULL AUTO_INCREMENT,
  `majorCode` varchar(45) DEFAULT NULL,
  `majorRequiredUnit` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`majorRequiredUnitsID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=42 ;

INSERT INTO `majorRequiredUnits` (`majorRequiredUnitsID`, `majorCode`, `majorRequiredUnit`) VALUES
(1, '4307', 'FIT1004'),
(2, '4307', 'FIT1031'),
(3, '4307', 'FIT1040'),
(4, '4307', 'FIT2001'),
(5, '4307', 'FIT2002'),
(6, '4307', 'FIT2003'),
(7, '4307', 'FIT3047'),
(8, '4307', 'FIT3048'),
(9, '4307ADN', 'FIT2005'),
(10, '4307ADN', 'FIT2009'),
(11, '4307ADN', 'FIT2020'),
(12, '4307ADN', 'FIT2081'),
(13, '4307ADN', 'FIT2034'),
(14, '4307ADN', 'FIT3031'),
(15, '4307ADN', 'FIT3037'),
(16, '4307ADN', 'FIT3046'),
(17, '4307BS', 'ETW1102'),
(18, '4307BS', 'FIT2005'),
(19, '4307BS', 'FIT2033'),
(20, '4307BS', 'FIT2081'),
(21, '4307BS', 'FIT3002'),
(22, '4307BS', 'FIT3019'),
(23, '4307BS', 'FIT3021'),
(24, '4307BS', 'FIT3031');



CREATE TABLE IF NOT EXISTS `majorsOffered` (
  `majorCode` varchar(10) NOT NULL,
  `majorName` varchar(1000) NOT NULL,
  PRIMARY KEY (`majorCode`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='This table is called majors offered holding all degree options';



INSERT INTO `majorsOffered` (`majorCode`, `majorName`) VALUES
('4307', 'BCIS No Majors'),
('4307ADN', 'BCIS ADN'),
('4307BS', 'BCIS BS');



CREATE TABLE IF NOT EXISTS `rules` (
  `ruleID` int(11) NOT NULL AUTO_INCREMENT,
  `ruleName` varchar(500) NOT NULL,
  `ruleDescription` varchar(2000) NOT NULL,
  `ruleValue` int(10) NOT NULL,
  PRIMARY KEY (`ruleID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;


INSERT INTO `rules` (`ruleID`, `ruleName`, `ruleDescription`, `ruleValue`) VALUES
(1, 'level1Rule', 'Student cannot have more than 60 first year level points', 60),
(2, 'level2Rule', 'Student must have 18 or more second year level points', 18),
(3, 'level3Rule', 'Student must have at least 36 third year level points', 36),
(4, 'levelAllRule', 'Students must have a total of at least 144 points', 144);



CREATE TABLE IF NOT EXISTS `staff` (
  `staffID` varchar(8) NOT NULL,
  `name` varchar(300) DEFAULT NULL,
  `surname` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`staffID`),
  UNIQUE KEY `staffID_UNIQUE` (`staffID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


INSERT INTO `staff` (`staffID`, `name`, `surname`, `email`) VALUES
('brab5', 'Abraham', 'Van der Vyver', 'braam.vandervyver@monash.edu'),
('dama8', 'Daniel', 'Maposa', 'daniel.maposa@monash.edu'),
('mani9', 'Mattheus', 'Niemands', 'mattheus.niemand@monash.edu'),
('modas1', 'Mohan', 'Das', 'mohan.das@monash.edu'),
('nema6', 'Neil', 'Manson', 'neil.manson@monash.edu'),
('olbe3', 'Oladayo', 'Bello', 'oladayo.bello@monash.edu'),
('pamu7', 'Paula', 'Murray', 'paula.murray@monash.edu'),
('shwa2', 'Sheelagh', 'Walton', 'sheelagh.walton@monash.edu'),
('stou4', 'Stella', 'Ouma', 'stelle.ouma@monash.edu');



CREATE TABLE IF NOT EXISTS `staffAllocation` (
  `allocID` int(11) NOT NULL AUTO_INCREMENT,
  `staffID` varchar(10) NOT NULL,
  `unitCode` varchar(10) NOT NULL,
  PRIMARY KEY (`allocID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=36 ;



INSERT INTO `staffAllocation` (`allocID`, `staffID`, `unitCode`) VALUES
(4, 'dama8', 'ETW1102'),
(5, 'shwa2', 'FIT1002'),
(6, 'brab5', 'FIT1004'),
(7, 'modas1', 'FIT1031'),
(8, 'shwa2', 'FIT1040'),
(9, 'stou4', 'FIT2001'),
(10, 'pamu7', 'FIT2002'),
(11, 'brab5', 'FIT2003'),
(12, 'shwa2', 'FIT2005'),
(13, 'modas1', 'FIT2009'),
(14, 'olbe3', 'FIT2020'),
(15, 'shwa2', 'FIT2029'),
(16, 'nema6', 'FIT2033'),
(17, 'shwa2', 'FIT2034'),
(18, 'mani9', 'FIT2081'),
(19, 'nema6', 'FIT3002'),
(20, 'olbe3', 'FIT3019'),
(21, 'shwa2', 'FIT3021'),
(22, 'olbe3', 'FIT3031'),
(23, 'stou4', 'FIT3037'),
(24, 'pamu7', 'FIT3046'),
(25, 'pamu7', 'FIT3047'),
(26, 'pamu7', 'FIT3048');


CREATE TABLE IF NOT EXISTS `units` (
  `unitCode` varchar(7) NOT NULL,
  `unitDescription` varchar(45) NOT NULL,
  PRIMARY KEY (`unitCode`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


INSERT INTO `units` (`unitCode`, `unitDescription`) VALUES
('ETW1102', 'Business Statistics'),
('FIT1002', 'Computer Programming 1'),
('FIT1004', 'Data management'),
('FIT1031', 'Computers and networks'),
('FIT1040', 'Programming fundamentals'),
('FIT2001', 'Systems development'),
('FIT2002', 'IT project management'),
('FIT2003', 'IT professional practise'),
('FIT2005', 'Software analysis, design and architecture'),
('FIT2009', 'Data structure and algorithms'),
('FIT2020', 'Network architecture'),
('FIT2029', 'Web programming'),
('FIT2033', 'Computer models for business decisions'),
('FIT2034', 'Computer programming 2'),
('FIT2081', 'Mobile application development'),
('FIT3002', 'Applications of data mining'),
('FIT3019', 'Information system management'),
('FIT3021', 'Infrastructure for E-commerce'),
('FIT3031', 'Information and network security'),
('FIT3037', 'Software engineering'),
('FIT3046', 'Operating environments'),
('FIT3047', 'Industrial experience project 1'),
('FIT3048', 'Industrial experience project 2');

