<?php
require_once (dirname(__FILE__) . '/mPDF/mpdf.php');

$addHTML = $_POST['planner'];
$fileName = '/var/www/html/tmp/' . $_POST['file'] . '.pdf';
//$tmpFile = fopen("tmp.pdf", "w");
convert($addHTML, $fileName);


function convert($contents, $file) {
	try {

		$mpdf = new mPDF('c', 'A4', '');

		$mpdf -> SetDisplayMode('fullpage');

		//$mpdf -> list_indent_first_level = 0;
		// 1 or 0 - whether to indent the first level of a list

		// LOAD a stylesheet
		$stylesheet = file_get_contents('css/planningForm.css');
		$mpdf -> WriteHTML($stylesheet, 1);
		// The parameter 1 tells that this is css/style only and no body/html/text

		$mpdf -> WriteHTML($contents);

		$mpdf -> Output($file, 'F');

		echo "success";
	} catch(HTML2PDF_exception $e) {
		echo "pdferror";
	}

}
?>