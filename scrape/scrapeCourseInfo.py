#!/usr/bin/python
from bs4 import BeautifulSoup
import re
import json
import cgitb
import cgi
cgitb.enable()

# Create instance of FieldStorage 
form = cgi.FieldStorage() 
path = form['thisName'].value
file = "/var/www/html/tmp/"
file += str(path)
#open file on server
f = open(file, "r+")
html = f.read();

#assign html to beautiful soup object 'soup'
soup = BeautifulSoup(html)

#find last th tag which will be present in GPA WAM table
th = soup.find_all('th')
lastTH =  len(th)


#get course titles gpa's wam's 
#find table by using last th tag
#find correct rows of that table using find-all tr's with class empty
#iterate through those rows and find all correct td's with colspan empty
#iterate through td's get data and push to courses array
lastTableRows = th[lastTH-1].findParent('table').find_all('tr',{"class":""})
tds = []
for tr in lastTableRows:
	tds += tr.find_all('td',{"colspan":""})

courses = []
i = 0
while i <= (len(tds)-2):
	newSet = {"courseTitle":tds[i].text, "GPA":tds[i+1].text, "WAM":tds[i+2].text}
	courses.append(newSet)
	i += 3

#option to store courses as JSON type file on server
#with open('courses.txt', 'w') as outfile:
#  json.dump(courses, outfile)
#outfile.close()

f.close()

print "Content-type: application/json\n\n";
print json.dumps(courses)

