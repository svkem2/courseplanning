#!/usr/bin/python
from bs4 import BeautifulSoup
import re
import json
import cgitb
import cgi
cgitb.enable()

# Create instance of FieldStorage 
form = cgi.FieldStorage() 
path = form['thisName'].value
file = "/var/www/html/tmp/"
file += str(path)
#open file on server
f = open(file, "r+")
html = f.read();

#assign html to beautiful soup object 'soup'
soup = BeautifulSoup(html)

#get the text inbetween the font tags
fontText = soup.body.font.text

#store words of fontText in list
studDetailsList = fontText.split()

#loop through list, find and store name and student number
welcomeFlag = False
dashFlag =  False
gotNum = False
studentName = ""
studentNumber = ""
for word in studDetailsList:
	
	if (welcomeFlag == True) and (dashFlag == False):
		if word != "-":
			studentName += word + " "

	if (dashFlag == True) and (gotNum == False):
		studentNumber = word[0:8]
		gotNum = True
		
	if word == "-":
		dashFlag = True

	if word == "Welcome":
		welcomeFlag = True

#store student details  records in JSON format in files
studentDetails = []
studentDetails.append({"studentName" : studentName, "studentNumber" : studentNumber})

#option to save details to file on server
#with open('student.txt', 'w') as outfile:
#  json.dump(studentDetails, outfile)
#outfile.close()

f.close()

print "Content-type: application/json\n\n";
print json.dumps(studentDetails)

