<?php session_start(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN"
"http://www.w3.org/TR/html4/strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Course Prerequisite Waiver</title>
		<meta name="author" content="stu" />
		<!-- CSS
		============================================================================= -->
		<link href="css/basic.css" rel="stylesheet" type="text/css" />
		<link href="css/waiverForm.css" rel="stylesheet" type="text/css" />
		<!-- SCRIPTS
		============================================================================= -->
		<script src="js/jquery-1.10.2.min.js"></script>
		<script src="js/waiver.js"></script>
	</head>
	<body>
		<div class="popUpWrapper hidden">
			<div class="popUp rounded">
				<div class="popUpHeader">
					<h1>Waiver Request Sent.</h1>
				</div>
				<div>
					<ul>
						<li>
							<p>
								Your request has been sent.
							</p>
						</li>
						<li>
							<p>
								This request will be responded to shortly.
							</p>
						</li>
					</ul>
				</div>
				<div id="waiverPopUpContinueButton" class="buttonArea">
					<a href="index.html"></a>
				<div class="button buttonFloatRight">
					<a href="index.html">
					<p>Exit</p></a>
				</div>
			</div>
			</div>
			
		</div>
		<div class="contentBox rounded dropShadow">

			<div id="header"></div>
			<div class="pageTitle">
				<p>
					<a href="index.html">Home </a><span>> </span><a href="waiverUnitSelect.php">Select Units <span>> </span></a> Prerequisite Waiver Request Form
				</p>
			</div>

			<div class="infoPanel rounded">
				<ul>
					<li>
						<p>
							The following is a complete prerequisite form for the subjects you selected.
						</p>
					</li>
					<li>
						<p>
							Please read through the entire document to ensure you agree with the terms and conditions.
						</p>
					</li>
					<li>
						<p>
							Please ensure the correct units have been selected. If any unit selections need to be changed please click the 'Go Back' button to make changes.
						</p>
					</li>
					<li>
						<p>
							Once you are satisfied with the contents of this document and accept the terms &amp; conditions, please click the 'Accept and Continue' button.
						</p>
					</li>
					<li>
						<p>
							Once you click 'Accept and Continue' the request will be processed.
						</p>
					</li>
				</ul>
			</div>

			<!-- WAIVER FORM : Wrapper for entire form -->
			<div id="waiverForm">
				
				<br/>
				<h2>&nbsp; Section A: Unit Information</h2>
				<br/>

				<ul class="bulletedInfo">
					<li>
						<p>
							<span class="strong">Summer winter units:</span> are charged at the full fee rate with limited exceptions see www.adm.monash.edu.au/enrolments and click on the links for summer or winter. It is your responsibility to check with your faculty if the unit is charged at the CSP or full-fee rate.
						</p>
					</li>
					<li>
						<p>
							<span class="strong">Distance education units:</span> can only be added up until two weeks before semester starts
						</p>
					</li>
					<li>
						<p>
							<span class="strong">On-campus units:</span> can only be added until two weeks after the start of semester. Units added after this time require faculty approval and may only be approved in extenuating circumstances. A fee will be payable if a late addition is approved. A list of fees is available at: www.monash.edu/fees/other/miscellaneous.html.
						</p>
					</li>
					<li>
						<p>
							All students are bound by the University’s principal dates for adding and discontinuing units. These can be found online at: www.monash.edu.au/pubs/sii/principal-dates-index.html
						</p>
					</li>
				</ul>
				<br/>

				<h2>&nbsp; Section B: Units Selected for Prerequisite Request</h2>
				<br/>
				<table class="innerTable">
					<tr>
						<td class="tableHeader">
						<p>
							Unit Code
						</p></td>
						<td class="tableHeader">
						<p>
							Unit Name
						</p></td>
					</tr>
					<tr>
						<td id="unit1code" class="tableData"><p><?php echo $_SESSION['unit1']; ?></p></td>
						<td id="unit1name" class="tableData"><p><?php echo $_SESSION['unit1Description']; ?></p></td>
					</tr>
					<tr>
						<td id="unit2code" class="tableData"><?php
						if ($_SESSION['unit2'] != "Please Select Unit")
							echo "<p>" . $_SESSION['unit2'] . "</p>";
					?></td>
						<td id="unit2name" class="tableData"><?php
						if ($_SESSION['unit2'] != "Please Select Unit")
							echo "<p>" . $_SESSION['unit2Description'] . "</p>";
						?></td>
					</tr>
					<tr>
						<td id="unit3code" class="tableData"><?php
						if ($_SESSION['unit3'] != "Please Select Unit")
							echo "<p>" . $_SESSION['unit3'] . "</p>";
					?></td>
						<td id="unit3name" class="tableData"><?php
						if ($_SESSION['unit3'] != "Please Select Unit")
							echo "<p>" . $_SESSION['unit3Description'] . "</p>";
						?></td>
					</tr>
				</table>
				<br/>
				<h2>&nbsp; Section C: Student’s acknowledgement, agreement and consent.</h2>
				<br/>
				<ul class="bulletedInfo">
					<li>
						<p>
							I have read the university's statement on privacy and the purposes for which my personal information will be used.
						</p>
					</li>
					<li>
						<p>
							I authorise the university to release personal information for educational purposes or to meet legal obligations or in the case of emergency.
						</p>
					</li>
					<li>
						<p>
							If tuition fees are paid by an organisation registered with Monash as a sponsor, I authorise the university to release fee and academic progress information to my sponsor.
						</p>
					</li>
					<li>
						<p>
							If under 18 years of age, I authorise the university to release academic progress information to my Monash approved caregiver.
						</p>
					</li>
					<li>
						<p>
							I agree to be bound by the statutes, regulations and policies of the university as amended from time to time and agree to pay all fees, levies and charges directly arising from my enrolment.
						</p>
					</li>
					<li>
						<p>
							I consent to receiving electronically, information and business documents relating to my enrolment from the university and from university owned companies that provide support services to students on behalf of the university.
						</p>
					</li>
					<li>
						<p>
							I agree to access the correspondence of my Monash University student email account on a regular basis.
						</p>
					</li>
				</ul>
				<br />

			</div><!-- end waiverForm -->

			<div id="acceptButton" class="buttonArea">
				<div id="requestWaiver" class="button buttonFloatRight">
					<p>Accept and Continue</p>
				</div>
				<div id="goBackButton" class="button buttonFloatLeft">
					<a href="waiverUnitSelect.php">
					<p>Go Back</p></a>
				</div>
			</div>

		</div><!-- end contentBox -->

	</body>
</html>

