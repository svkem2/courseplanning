<?php

require ("loginTest.php");

//check if staff id exists
$query = "
SELECT
*
FROM staffAllocation
WHERE
staffID = :staffID
AND
unitCode = :unitCode
";

$query_params = array(':unitCode' => $_POST['unitCode'], ':staffID' => $_POST['staffID']);

try {

	$stmt = $db -> prepare($query);
	$result = $stmt -> execute($query_params);
} catch (PDOException $ex) {

	die("error");
}

$row = $stmt -> fetch();

if ($row) {
	die("allocationExists");
}


$query = "
INSERT INTO staffAllocation (
staffID,
unitCode
) VALUES (
:staffID,
:unitCode
)
";



$query_params = array(':unitCode' => $_POST['unitCode'], ':staffID' => $_POST['staffID']);

try { 

	$stmt = $db -> prepare($query);
	$result = $stmt -> execute($query_params);
	die("success");
} catch (PDOException $ex) {

	die("error");
}

?>
