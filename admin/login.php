<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN"
"http://www.w3.org/TR/html4/strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Welcome To Course Planning, Monash School of IT, South Africa</title>
		<meta name="author" content="stu" />
		<!-- CSS
		============================================================================= -->
		<link href="../css/basic.css" rel="stylesheet" type="text/css" />
		<link href="../admin/css/login.css" rel="stylesheet" type="text/css" />

		<!-- SCRIPTS
		============================================================================= -->
		<script src="js/jquery-1.11.0.min.js"></script>
		<script src="js/login.js"></script>

	</head>
	<div class="javascriptPopUp">
			<div class="popUp rounded">
				<div class="popUpHeader">
					<h1>Javascript is disabled!</h1>
				</div>
				<div>
					<ul>
						<li>
							<p>
								This site requires javascript to work correctly.
							</p>
						</li>
						<li>
							<p>
								Please enable javascript or use a browser that supports javascript.
							</p>
						</li>
						<li>
							<p>
								Once javascript is enabled please refresh the page to continue.
							</p>
						</li>
					</ul>
				</div>
				

			</div>
			
		</div>
	<body>
		<div class="contentBox rounded dropShadow">
			<div id="header"></div>
			<div class="pageTitle">
				<p>
					<a>Administration Login</a>
				</p>
			</div>
		
			<div class="formCentre">
				<h1>Login</h1>
				<div id="loginForm" >
					<input type="text" id="userName" name="username" placeholder="Admin Username" value="" />
					<input type="password" id="userPassword" name="password" placeholder="Admin Password" value="" />
					<div id="errors">
						<p id="usernameError" class="errorText" ></p>
						<p id="passwordError" class="errorText" ></p>
					</div>
				</div>

			</div>
			<!-- Buttons -->
			<div  class="buttonArea">

				<div id="loginButton" class="button buttonFloatRight">
					<a>
					<p>
						Login
					</p></a>
				</div>
			</div>

		</div><!-- end contentBox -->
	</body>
</html>