<?php
require("common.php");
if (empty($_SESSION['user'])) {
    header("Location: login.php");
    die("Redirecting to login.php");
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN"
"http://www.w3.org/TR/html4/strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Course Planning Administration</title>
		<meta name="author" content="stu" />
		<!-- CSS
		============================================================================= -->
		<link href="../css/basic.css" rel="stylesheet" type="text/css" />
		
		<link href="../admin/css/adminHome.css" rel="stylesheet" type="text/css" />
		<!-- SCRIPTS
		============================================================================= -->
		<script src="js/jquery-1.10.2.min.js"></script>
		<script src="js/adminHome.js"></script>
	</head>
	<body>
		<!-- javascript requirment popup -->
		<div class="javascriptPopUp">
			<div class="popUp rounded">
				<div class="popUpHeader">
					<h1>Javascript is disabled!</h1>
				</div>
				<div>
					<ul>
						<li>
							<p>
								This site requires javascript to work correctly.
							</p>
						</li>
						<li>
							<p>
								Please enable javascript or use a browser that supports javascript.
							</p>
						</li>
						<li>
							<p>
								Once javascript is enabled please refresh the page to continue.
							</p>
						</li>
					</ul>
				</div>
				

			</div>
			
		</div>
		<div class="contentBox rounded dropShadow">
			<div id="header"></div>
			<div class="pageTitle">
				<p>
					<a style="color: #124c8a">Admin Dashboard</a><span id="logout"><a href="logout.php">Logout</a></span>
				</p>
			</div>
			<div id="adminContent">
				<h1>Welcome <?php echo htmlentities($_SESSION['user']['username'], ENT_QUOTES, 'UTF-8'); ?>,</h1>
				<div id="menuButtons">
					
					<div id="manageStaff" class="menuButton">
						<div class="menuImage staff"></div>
						<div class="menuText">
							<p>Manage Staff</p>
						</div>
					</div>
					
					
					<div id="manageStaffAllocations" class="menuButton">
						<div class="menuImage allocations"></div>
						<div class="menuText">
							<p>Manage Staff Allocations</p>
						</div>
					</div>
					
					<div id="manageSubjects" class="menuButton">
						<div class="menuImage books"></div>
						<div class="menuText">
							<p>Manage Subjects</p>
						</div>
					</div>
					
					<div id="manageMajors" class="menuButton">
						<div class="menuImage manageMajor"></div>
						<div class="menuText">
							<p>Manage Majors</p>
						</div>
					</div>
					
					<div id="manageMajorUnits" class="menuButton">
						<div class="menuImage manageMajorUnits"></div>
						<div class="menuText">
							<p>Manage Core Units</p>
						</div>
					</div>
					
					<div id="manageRules" class="menuButton">
						<div class="menuImage manageRules"></div>
						<div class="menuText">
							<p>Manage Course Rules</p>
						</div>
					</div>
					
					<div id="registerUser" class="menuButton">
						<div class="menuImage user"></div>
						<div class="menuText">
							<p>Manage Admin Users</p>
						</div>
					</div>
					
					<div id="backupDatabase" class="menuButton">
						<div class="menuImage backup"></div>
						<div class="menuText">
							<p>Backup Database</p>
						</div>
					</div>
					
				</div>
			</div>
			
		</div><!-- end contentBox -->

	</body>
</html>

