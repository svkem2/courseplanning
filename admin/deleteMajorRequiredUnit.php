<?php

require("loginTest.php");

$majorCode = $_POST['major'];
$majorRequiredUnit = $_POST['unit'];


$query = "DELETE FROM majorRequiredUnits 
          WHERE majorCode = :major 
          AND majorRequiredUnit = :unit ";



$query_params = array(':major' => $majorCode, ':unit' => $majorRequiredUnit);

try { 

	$stmt = $db -> prepare($query);
	$result = $stmt -> execute($query_params);
	die("success");
} catch (PDOException $ex) {

	die("error");
}

?>
