<?php

require("loginTest.php");

$major = $_POST['majorValue'];
$unit = $_POST['unitValue'];

//check first if major unit combo exists

$query = "SELECT 1 FROM majorRequiredUnits 
          WHERE majorCode = :majorID
          AND majorRequiredUnit = :unitID";



$query_params = array(':majorID' => $major, ':unitID' => $unit);

try { 

	$stmt = $db -> prepare($query);
	$result = $stmt -> execute($query_params);
} catch (PDOException $ex) {

	die("error");
}

$row = $stmt -> fetch();

if ($row) {
	die("requirementExists");
}


$query = "INSERT INTO majorRequiredUnits 
          (majorCode, majorRequiredUnit)
          VALUES (:majorID, :unitID)";



$query_params = array(':majorID' => $major, ':unitID' => $unit);

try { 

	$stmt = $db -> prepare($query);
	$result = $stmt -> execute($query_params);
	die("success");
} catch (PDOException $ex) {

	die("error");
}

?>
