
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN"
"http://www.w3.org/TR/html4/strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Course Planning Administration</title>
		<meta name="author" content="stu" />
		<!-- CSS
		============================================================================= -->
		<link href="../css/basic.css" rel="stylesheet" type="text/css" />
		<link href="../admin/css/registerUser.css" rel="stylesheet" type="text/css" />

		<!-- SCRIPTS
		============================================================================= -->
		<script src="js/jquery-1.10.2.min.js"></script>
		<script src="js/userRegister.js"></script>

	</head>
	<body>
		<div class="popUpWrapper hidden">
			<div class="popUp rounded">
				<div class="popUpHeader">
					<h1>New Admin User Created</h1>
				</div>
				<div id="popUpText">
				</div>
				<div class="buttonArea">
					<div id="popGoBack" class="button buttonFloatLeft">
						<a href="adminHome.php">
						<p>
							Go Back
						</p></a>
					</div>
					<div id="popRegisterAnother" class="button buttonFloatRight">
						<a href="registerUser.php">
						<p>
							Register Another User
						</p></a>
					</div>
				</div>
			</div>

		</div>

		<div class="contentBox rounded dropShadow">
			<div id="header"></div>
			<div class="pageTitle">
				<p>
					<a href="adminHome.php">Admin Dashboard </a><span>> </span>Register Admin User <span id="logout"><a href="logout.php">Logout</a></span>
				</p>
			</div>
			<div id="adminContent">
				<div class="formCentre">
					<h1>Register Admin User</h1>
					<div id="registerForm" >
						<input id="userName" type="text" name="username" placeholder="Admin Username" value="" />
						<input id="userEmail" type="text" name="email" placeholder="Admin Email" value="" />
						<input id="userPassword" type="password" name="password" placeholder="Admin Password" value="" />
						<div id="errors">
							<p id="usernameError" class="errorText" ></p>
							<p id="emailError" class="errorText" ></p>
							<p id="passwordError" class="errorText" ></p>
						</div>
					</div>
				</div>
				<div class="buttonArea">
					<div id="goBack" class="button buttonFloatLeft">
						<a href="adminHome.php">
						<p>
							Go Back
						</p></a>
					</div>
					<div id="registerUser" class="button buttonFloatRight">
						<a>
						<p>
							Register User
						</p></a>
					</div>
				</div>
			</div>

		</div><!-- end contentBox -->

	</body>
</html>

