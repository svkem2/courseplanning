<?php

require ("loginTest.php");

//check if staff id exists
$query = "
SELECT
1
FROM staff
WHERE
staffID = :staffID
";

$query_params = array(':staffID' => $_POST['staffID']);

try {

	$stmt = $db -> prepare($query);
	$result = $stmt -> execute($query_params);
} catch (PDOException $ex) {

	die("error");
}

$row = $stmt -> fetch();

if ($row) {
	die("staffIDExists");
}

$query = "
SELECT
1
FROM staff
WHERE
email = :email
";

$query_params = array(':email' => $_POST['email']);

try {
	$stmt = $db -> prepare($query);
	$result = $stmt -> execute($query_params);
} catch (PDOException $ex) {
	die("error");
}

$row = $stmt -> fetch();

if ($row) {
	die("emailExists");
}


$query = "
INSERT INTO staff (
staffID,
name,
surname,
email
) VALUES (
:staffID,
:name,
:surname,
:email
)
";



$query_params = array(':staffID' => $_POST['staffID'], ':name' => $_POST['name'], ':surname' => $_POST['surname'], ':email' => $_POST['email']);

try { 

	$stmt = $db -> prepare($query);
	$result = $stmt -> execute($query_params);
	die("success");
} catch (PDOException $ex) {

	die("error");
}

?>
