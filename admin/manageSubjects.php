<?php
require("common.php");
if (empty($_SESSION['user'])) {
    header("Location: login.php");
    die("Redirecting to login.php");
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN"
"http://www.w3.org/TR/html4/strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Course Planning Administration</title>
		<meta name="author" content="stu" />
		<!-- CSS
		============================================================================= -->
		<link href="../css/basic.css" rel="stylesheet" type="text/css" />
		<link href="../admin/css/manageSubjects.css" rel="stylesheet" type="text/css" />

		<!-- SCRIPTS
		============================================================================= -->
		<script src="js/jquery-1.10.2.min.js"></script>
		<script src="js/manageSubjects.js"></script>

	</head>
	<body>
		

		<div class="contentBox rounded dropShadow">
			<div id="header"></div>
			<div class="pageTitle">
				<p>
					<a href="adminHome.php">Admin Dashboard </a><span>> </span>Manage Subjects <span id="logout"><a href="logout.php">Logout</a></span>
				</p>
			</div>
			<div id="manageSubjectsContent">
				<h1>Current Units</h1>
				<div id="unitsList">
				</div>
				
				
			</div>
			<div class="buttonArea">
					<div id="goBack" class="button buttonFloatLeft">
						<a href="adminHome.php">
						<p>
							Go Back
						</p></a>
					</div>
					
				</div>

		</div><!-- end contentBox -->

	</body>
</html>

