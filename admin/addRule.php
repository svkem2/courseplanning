<?php

require ("loginTest.php");


$query = "
SELECT
1
FROM rules
WHERE
ruleName = :ruleName
";

$query_params = array(':ruleName' => $_POST['ruleName']);

try {
	$stmt = $db -> prepare($query);
	$result = $stmt -> execute($query_params);
} catch (PDOException $ex) {
	die("error");
}

$row = $stmt -> fetch();

if ($row) {
	die("nameExists");
}


$query = "
INSERT INTO rules (
ruleID,
ruleName,
ruleDescription,
ruleValue
) VALUES (
NULL,
:ruleName,
:ruleDescription,
:ruleValue
)
";



$query_params = array(':ruleName' => $_POST['ruleName'], ':ruleDescription' => $_POST['ruleDescription'], ':ruleValue' => $_POST['ruleValue']);

try { 

	$stmt = $db -> prepare($query);
	$result = $stmt -> execute($query_params);
	die("success");
} catch (PDOException $ex) {

	die("error");
}

?>
