<?php
require ("common.php");
if (empty($_SESSION['user'])) {
	header("Location: login.php");
	die("Redirecting to login.php");
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN"
"http://www.w3.org/TR/html4/strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Course Rule Management</title>
		<meta name="Brad" content="Brad" />
		<!-- CSS
		============================================================================= -->
		<link href="../css/basic.css" rel="stylesheet" type="text/css" />
		<link href="../admin/css/manageRule.css" rel="stylesheet" type="text/css" /> 

		<!-- SCRIPTS
		============================================================================= -->
		<script src="js/jquery-1.10.2.min.js"></script>
		<script src="js/manageRule.js"></script> 

	</head>
	<body>
		<div class="popUpWrapper hidden">
			<div class="popUp rounded">
				<div class="popUpHeader">
					<h1>New Course Rule Created</h1>
				</div>
				<div id="popUpText"></div>
				<div class="buttonArea">
					<div id="button1" class="button buttonFloatLeft">
						<a href="adminHome.php">
						<p>
							Go Back
						</p></a>
					</div>
					<div id="button2" class="button buttonFloatRight">
						<a href="registerUser.php"> 
						<p>
							Register Another User
						</p></a>
					</div>
				</div>
			</div>

		</div>

		<div class="contentBox rounded dropShadow">
			<div id="header"></div>
			<div class="pageTitle">
				<p>
					<a href="adminHome.php">Admin Dashboard </a><span>> </span>Manage Course Rules <span id="logout"><a href="logout.php">Logout</a></span>
				</p>
			</div>
			<div id="manageRules">
				<h1>Course Rule Management</h1>
				<div id="searchBoxDiv">
					<input id="searchBox" class="searchBox rounded" placeholder="Enter Search Term/s" value=""/>
					<p>Search for Course Rule:</p>
				</div>
				<div id="rulesList">
					<table id="rulesTable"></table>
				</div>

			</div>
			<div class="buttonArea">
				<div id="goBack" class="button buttonFloatLeft">
					<a href="adminHome.php">
					<p>
						Go Back
					</p></a>
				</div>

			</div>

		</div><!-- end contentBox -->

	</body>
</html>

