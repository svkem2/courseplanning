<?php

require ("loginTest.php");

//check if staff id exists
$query = "
SELECT
*
FROM majorsOffered
WHERE
majorCode = :majorCode
";

$query_params = array(':majorCode' => $_POST['majorCode']);

try {

	$stmt = $db -> prepare($query);
	$result = $stmt -> execute($query_params);
} catch (PDOException $ex) {

	die("error");
}

$row = $stmt -> fetch();

if ($row) {
	die("majorExists");
}

$query = "
SELECT
*
FROM majorsOffered
WHERE
majorName = :majorName
";

$query_params = array(':majorName' => $_POST['majorName']);

try {
	$stmt = $db -> prepare($query);
	$result = $stmt -> execute($query_params);
} catch (PDOException $ex) {
	die("error");
}

$row = $stmt -> fetch();

if ($row) {
	die("nameExists");
}


$query = "
INSERT INTO majorsOffered (
majorCode,
majorName
) VALUES (
:majorCode,
:majorName
)
";



$query_params = array(':majorCode' => $_POST['majorCode'], ':majorName' => $_POST['majorName']);

try { 

	$stmt = $db -> prepare($query);
	$result = $stmt -> execute($query_params);
	die("success");
} catch (PDOException $ex) {

	die("error");
}

?>
