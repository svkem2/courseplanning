$(document).ready(function() {
	
	//first load populateStaff this loads staff into table structure
	populate($("#searchBox").val());
	
	//hide any popups
	$(".popUpWrapper").hide();

	function populate(filter) {
		//get array of units from database
		//iterate and populate table
		html = '<tr><th>Major Code</th><th>Major Name</th><th>Update</th><th>Delete</th></tr>';
		var url = 'grabMajors.php';
		searchTerms = {"searchTerms" : filter};
		$.getJSON(url, searchTerms, function(data) {
				html += '<tr id="addNewUnit">';
				html += '<td><input id="newMajorCode" class="majorCode" type="text" name="majorCode" placeholder="Enter New Major Code" value="" /></td>';
				html += '<td><input id="newMajorName" type="text" name="majorName" placeholder="Enter New Major Name" value="" /></td>';
				html += '<td><input id="addNew" type="button" name="addNewButton" value="Add New" /></td>';
				html += '<td></td>';
				html += '</tr>';
			$.each(data, function(index, data) {
				html += '<tr id="' + data.majorCode + '">';
				html += '<td><input id="' + data.majorCode + 'majorCode" class="majorCode" type="text" name="majorCode" value="' + data.majorCode + '" /></td>';
				html += '<td><input id="' + data.majorCode + 'majorName" type="text" name="majorName" value="' + data.majorName + '" /></td>';
				html += '<td><input id="' + data.majorCode + 'update" class="listButton" type="button" name="update" value="Update Changes""/></td>';
				html += '<td><input id="' + data.majorCode + 'delete" class="listButton" type="button" name="delete" value="Delete" /></td>';
				html += '</tr>';
				$('#majorsTable').html(html);
			});
		});

	}
	
	//update staff table using filter of search terms from searchBox
	
	$("#searchBox").keyup(function(){
		populate($(this).val());
		
	});

	//listen to clicks on class listButton iether delete or update 

	$(document.body).on('click', '.listButton', function(e) {
		e.preventDefault();
		action = $(this).attr('name');
		majorCode = $(this).parent().parent().attr('id');
		if (action == 'update') {
			updateUnit(majorCode);
		}
		if (action == 'delete') {
			deleteUnit(majorCode);
		}
	});

	//delete staff

	function deleteUnit(code) {
		name = $("#" + code + "majorName").val();
		var result = confirm('Are you sure you want to delete ' + name + "?");
		if (result == true) {
			$.post("deleteMajor.php", {
				majorCode : code
			}, function(result) {
				if ($.trim(result) == "error") {
					alert("System Error, please contact administrator.");
					populate($("#searchBox").val());
				}

				if ($.trim(result) == "success") {
					//showPopUp(code+" "+name+" successfully deleted.","adminHome.php","Admin Home","manageMajors.php","Go Back");
					populate($("#searchBox").val());
				}

			});
		}

	}

	//update staff

	function updateUnit(oldCode) {
		majorCode = $("#" + oldCode + "majorCode").val();
		majorName = $("#" + oldCode + "majorName").val();
		type = "update";
		//validate staff details before update
		if (validateUnit(oldCode, majorCode, majorName, type)) {

			//update staff
			$.post("updateMajor.php", {
				oldCode : oldCode,
				majorCode : majorCode,
				majorName : majorName
			}, function(result) {
				if ($.trim(result) == "error") {
					alert("System error, please contact course planning directly");
				}

				if ($.trim(result) == "success") {
					showPopUp(majorCode+" "+majorName+" updated successfully!","adminHome.php","Admin Home","manageMajors.php","Go Back");
					populate($("#searchBox").val());
				}

			});

		}

	}

	//validate staff details for database updates

	function validateUnit(oldCode, code, description, type) {
		error = false;
		$("#" + code).css("background-image", "none");
		//if type update then check and report error against user id
		if (type == "update") {

			//if id empty
			if (code == "") {
				$("#" + oldCode + "majorCode").parent().css("background-image", "url('../admin/img/invalid.png')");
				error = true;
			}

			//if name empty and only characters

			if (description == "") {
				$("#" + oldCode + "majorName").parent().css("background-image", "url('../admin/img/invalid.png')");
				error = true;
			}

			//check for any errors reported

			if (error) {
				return false;
			} else {
				return true;
			}

		}
		//if adding new major check and report error against newStaffID new.. etc
		else if (type == "new") {
			//reset font to black
			$("#addNewUnit").css("color", "black");
			//if id empty
			if (code == "") {
				$("#newMajorCode").parent().css("background-image", "url('../admin/img/invalid.png')");
				error = true;
			}

			//if name empty and only characters

			if (description == "") {
				$("#newMajorName").parent().css("background-image", "url('../admin/img/invalid.png')");
				error = true;
			}

			//check for any errors reported

			if (error) {
				return false;
			} else {
				return true;
			}

		}
	}

	//remove invalid background image when focused on
	$(document.body).on('click', 'td', function(e) {
		$(this).css("background-image", "none");
	});

	//add new staff

	$(document.body).on('click', '#addNew', function(e) {
		e.preventDefault();
		majorCode = $("#newMajorCode").val();
		majorName = $("#newMajorName").val();
		type = "new";
		//validate inputs
		if (validateUnit(majorCode, majorCode, majorName, type)) {
			//add new staff to database
			$.post("addMajor.php", {
				majorCode : majorCode,
				majorName : majorName
			}, function(result) {
				if ($.trim(result) == "error") {
					alert("error");
				}

				if ($.trim(result) == "success") {
					populate($("#searchBox").val());
					//showPopUp(majorCode+" added successfully!","adminHome.php","Admin Home","manageMajors.php","Go Back");
				}

				if ($.trim(result) == "majorExists") {
					showPopUp(majorCode+" already exists!","adminHome.php","Admin Home","manageMajors.php","Go Back");
				}

				if ($.trim(result) == "majorName") {
					showPopUp(majorName+" already exists!","adminHome.php","Admin Home","manageMajors.php","Go Back");
				}
				
				if ($.trim(result) == "error") {
					showPopUp(unitDescription+" System Error Please Contact Administrator","adminHome.php","Admin Home","manageUnits.php","Go Back");
				}

			});
		}
	});
	
	//takes variables builds and displays popup
	function showPopUp (message, button1Href, button1Msg, button2Href, button2Msg){
		messageHTML = '<h1>'+message+'</h1>';
		button1HTML = '<a href="'+button1Href+'"><p>'+button1Msg+'</p></a>';
		button2HTML = '<a href="'+button2Href+'"><p>'+button2Msg+'</p></a>';
		
		// add message
		$(".popUpHeader").html(messageHTML);
		// add button 1
		$("#button1").html(button1HTML);
		//add button 2
		$("#button2").html(button2HTML);
		//display popup
		$(".popUpWrapper").fadeIn();		
		
	}

	$("#goBack").click(function() {
		$(location).attr('href', "http://localhost/cp/admin/adminHome.php");
	});

});
