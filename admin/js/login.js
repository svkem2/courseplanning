$(document).ready(function() {
	var username;
	var password;
	$(".javascriptPopUp").hide();
	$("#loginButton").click(function() {
		username = ($("#userName").val());
		password = ($("#userPassword").val());
		$("#usernameError").text("");
		$("#passwordError").text("");
		if (validate(username, password)) {
			$.post("loginScript.php", {
				username : username,
				password : password
			}, function(result) {
				if($.trim(result) == "usernameError"){
					$("#usernameError").text("*User does not exist");
					$("#usernameError").show();
				}
				
				if($.trim(result )== "passwordError"){
					$("#passwordError").text("*Incorrect password");
					$("#passwordError").show();
				}
				
				if($.trim(result) == "loggedIn"){
					$("#usernameError").text("");
		            $("#passwordError").text("");	            
					$(location).attr('href',"http://localhost/cp/admin/adminHome.php");
				}
				
			});

		}
	});

	function validate(usr, pass) {
		if (usr != "" && pass != "") {
			return true;
		} else {
			if (usr == "") {
				$("#usernameError").text("*Please enter a username");
				$("#usernameError").show();
			}

			if (pass == "") {
				$("#passwordError").text("*Please enter a password");
				$("#passwordError").show();
			}
			
			return false;
		}
	}

});
