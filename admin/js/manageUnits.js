$(document).ready(function() {
	
	//first load populateStaff this loads staff into table structure
	populateUnits($("#searchBox").val());
	
	//hide any popups
	$(".popUpWrapper").hide();

	function populateUnits(filter) {
		//get array of units from database
		//iterate and populate table
		html = '<tr><th>Unit Code</th><th>Unit Name</th><th>Update</th><th>Delete</th></tr>';
		var url = 'grabUnitsPlusName.php';
		searchTerms = {"searchTerms" : filter};
		$.getJSON(url, searchTerms, function(data) {
				html += '<tr id="addNewUnit">';
				html += '<td><input id="newUnitCode" class="unitCode" type="text" name="unitCode" placeholder="Enter New Unit Code" value="" /></td>';
				html += '<td><input id="newUnitDescription" type="text" name="unitDescription" placeholder="Enter New Unit Name" value="" /></td>';
				html += '<td><input id="addNew" type="button" name="addNewButton" value="Add New" /></td>';
				html += '<td></td>';
				html += '</tr>';
			$.each(data, function(index, data) {
				html += '<tr id="' + data.unitCode + '">';
				html += '<td><input id="' + data.unitCode + 'unitCode" class="unitCode" type="text" name="unitCode" value="' + data.unitCode + '" /></td>';
				html += '<td><input id="' + data.unitCode + 'unitDescription" type="text" name="unitDescription" value="' + data.unitDescription + '" /></td>';
				html += '<td><input id="' + data.unitCode + 'update" class="listButton" type="button" name="update" value="Update Changes""/></td>';
				html += '<td><input id="' + data.unitCode + 'delete" class="listButton" type="button" name="delete" value="Delete" /></td>';
				html += '</tr>';
				$('#unitsTable').html(html);
			});
		});

	}
	
	//update staff table using filter of search terms from searchBox
	
	$("#searchBox").keyup(function(){
		populateUnits($(this).val());
		
	});

	//listen to clicks on class listButton iether delete or update 

	$(document.body).on('click', '.listButton', function(e) {
		e.preventDefault();
		action = $(this).attr('name');
		unitCode = $(this).parent().parent().attr('id');
		if (action == 'update') {
			updateUnit(unitCode);
		}
		if (action == 'delete') {
			deleteUnit(unitCode);
		}
	});

	//delete staff

	function deleteUnit(code) {
		name = $("#" + code + "unitDescription").val();
		var result = confirm('Are you sure you want to delete ' + name + "?");
		if (result == true) {
			$.post("deleteUnit.php", {
				unitCode : code
			}, function(result) {
				if ($.trim(result) == "error") {
					alert("error");
					populateUints();
				}

				if ($.trim(result) == "success") {
					//showPopUp(code+" "+name+" successfully deleted.","adminHome.php","Admin Home","manageUnits.php","Go Back");
					populateUnits($("#searchBox").val());
				}

			});
		}

	}

	//update staff

	function updateUnit(oldCode) {
		unitCode = $("#" + oldCode + "unitCode").val().toUpperCase();
		unitDescription = $("#" + oldCode + "unitDescription").val();
		type = "update";
		//validate staff details before update
		if (validateUnit(oldCode, unitCode, unitDescription, type)) {

			//update staff
			$.post("updateUnit.php", {
				oldCode : oldCode,
				unitCode : unitCode,
				unitDescription : unitDescription
			}, function(result) {
				if ($.trim(result) == "error") {
					alert("error, please get hold of Geoffry");
				}

				if ($.trim(result) == "success") {
					showPopUp(unitCode+" "+unitDescription+" updated successfully!","adminHome.php","Admin Home","manageUnits.php","Go Back");
					populateUnits($("#searchBox").val());
				}

			});

		}

	}

	//validate staff details for database updates

	function validateUnit(oldCode, code, description, type) {
		error = false;
		$("#" + code).css("background-image", "none");
		//if type update then check and report error against user id
		if (type == "update") {

			//if id empty
			if (code == "") {
				$("#" + oldCode + "unitCode").parent().css("background-image", "url('../admin/img/invalid.png')");
				error = true;
			}

			//if name empty and only characters

			if (description == "") {
				$("#" + oldCode + "unitDescription").parent().css("background-image", "url('../admin/img/invalid.png')");
				error = true;
			}

			//check for any errors reported

			if (error) {
				return false;
			} else {
				return true;
			}

		}
		//if adding new staff check and report error against newStaffID new.. etc
		else if (type == "new") {
			//reset font to black
			$("#addNewUnit").css("color", "black");
			//if id empty
			if (code == "") {
				$("#newUnitCode").parent().css("background-image", "url('../admin/img/invalid.png')");
				error = true;
			}

			//if name empty and only characters

			if (description == "") {
				$("#newUnitDescription").parent().css("background-image", "url('../admin/img/invalid.png')");
				error = true;
			}

			//check for any errors reported

			if (error) {
				return false;
			} else {
				return true;
			}

		}
	}

	//remove invalid background image when focused on
	$(document.body).on('click', 'td', function(e) {
		$(this).css("background-image", "none");
	});

	//add new staff

	$(document.body).on('click', '#addNew', function(e) {
		e.preventDefault();
		unitCode = $("#newUnitCode").val().toUpperCase();
		unitDescription = $("#newUnitDescription").val();
		type = "new";
		//validate inputs
		if (validateUnit(unitCode, unitCode, unitDescription, type)) {

			//add new staff to database
			$.post("addUnit.php", {
				unitCode : unitCode,
				unitDescription : unitDescription
			}, function(result) {
				if ($.trim(result) == "error") {
					alert("error");
				}

				if ($.trim(result) == "success") {
					populateUnits($("#searchBox").val());
					//showPopUp(unitCode+" added successfully!","adminHome.php","Admin Home","manageUnits.php","Go Back");
				}

				if ($.trim(result) == "unitExists") {
					showPopUp(unitCode+" already exists!","adminHome.php","Admin Home","manageUnits.php","Go Back");
				}

				if ($.trim(result) == "descriptionExists") {
					showPopUp(unitDescription+" already exists!","adminHome.php","Admin Home","manageUnits.php","Go Back");
				}
				
				if ($.trim(result) == "error1") {
					showPopUp(unitDescription+" tried to insert!","adminHome.php","Admin Home","manageUnits.php","Go Back");
				}

			});
		}
	});
	
	//takes variables builds and displays popup
	function showPopUp (message, button1Href, button1Msg, button2Href, button2Msg){
		messageHTML = '<h1>'+message+'</h1>';
		button1HTML = '<a href="'+button1Href+'"><p>'+button1Msg+'</p></a>';
		button2HTML = '<a href="'+button2Href+'"><p>'+button2Msg+'</p></a>';
		
		// add message
		$(".popUpHeader").html(messageHTML);
		// add button 1
		$("#button1").html(button1HTML);
		//add button 2
		$("#button2").html(button2HTML);
		//display popup
		$(".popUpWrapper").fadeIn();		
		
	}

	$("#goBack").click(function() {
		$(location).attr('href', "http://localhost/cp/admin/adminHome.php");
	});

});
