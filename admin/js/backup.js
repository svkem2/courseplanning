$(document).ready(function() {

	//hide any popups
	$(".popUpWrapper").hide();


	$("#goBack").click(function() {
		$(location).attr('href', "http://localhost/cp/admin/adminHome.php");
	});
	
	$("#goHome").click(function() {
		$(location).attr('href', "http://localhost/cp/admin/adminHome.php");
	});

	$("#backup").click(function() {
		$.get("backup.php", function(data, status) {
			if(data == "success"){
				//display popup
		    $(".popUpWrapper").fadeIn();
			}
			else {
				alert("System error. Please contact administration");
			}
		});
	});

});
