$(document).ready(function() {
	var username;
	var password;
	var email;
	$("#popUpWrapper").hide();
	$("#usernameError").text("");
	$("#passwordError").text("");
	$("#emailError").text("");

	$("#registerUser").click(function() {
		username = ($("#userName").val());
		password = ($("#userPassword").val());
		userEmail = ($("#userEmail").val());
		$("#usernameError").text("");
		$("#passwordError").text("");
		$("#emailError").text("");
		if (validate(username, password, userEmail)) {
			$.post("addAdminUser.php", {
				username : username,
				password : password,
				email : userEmail
			}, function(result) {
				if ($.trim(result) == "userExists") {
					$("#usernameError").text("*Username already exists");
					$("#usernameError").show();
				}

				if ($.trim(result) == "emailExists") {
					$("#emailError").text("*Email already exists");
					$("#emailError").show();
				}

				if ($.trim(result) == "success") {
					greatSuccess(username);
				}
				if ($.trim(result) == "error"){
					alert("Database Error, please notify Jeremy, youll find him in his rooms.");
				}

			});

		}
	});
	
	function greatSuccess(usr){
		popUpText = '<ul><li><p>User, '+usr+', hass been succesfully created</p></li><li><p>You can choose to create another user, or go back.</p></li></ul>';
		$("#popUpText").html(popUpText);
		$(".popUpWrapper").fadeIn();
	}

	function validate(usr, pass, email) {
		
		var validUser = false;
		var validEmail = false;
		var vaildPass = false;
		
		if (usr == "") {
			$("#usernameError").text("*Please enter a username");
			$("#usernameError").show();
		}
		else  validUser = true;
		
		if (pass == "") {
			$("#passwordError").text("*Please enter a password");
			$("#passwordError").show();
		}
		else validPass = true;
		
		var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
		if (testEmail.test(email)){
			validEmail = true;
		}
		else {
			$("#emailError").text("*Invalid Email");
			$("#emailError").show();    
		}
		
		if(validEmail && validPass && validUser){
			return true;
		}
		else {
			return false;
		}

	}
	
	//button listeners 
	
	$("#goBack, #popGoBack").click(function(){
		$(location).attr('href',"http://localhost/cp/admin/adminHome.php");
	});
	
	$("#popRegisterAnother").click(function(){
		$(location).attr('href',"http://localhost/cp/admin/registerUser.php");
	});

});
