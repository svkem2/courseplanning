$(document).ready(function() {
	
	//first load populateStaff this loads staff into table structure
	populateStaff($("#searchBox").val());
	
	//hide any popups
	$(".popUpWrapper").hide();

	function populateStaff(filter) {
		//get array of units from database
		//iterate and populate table
		html = '<tr><th>Staff ID</th><th>Name</th><th>Surname</th><th>Email</th><th></th><th></th></tr>';
		var url = 'grabStaff.php';
		searchTerms = {"searchTerms" : filter};
		$.getJSON(url, searchTerms, function(data) {
				html += '<tr id="addNewStaff">';
				html += '<td><input id="newStaffID" class="id" type="text" name="staffID" placeholder="Enter New Staff ID" value="" /></td>';
				html += '<td><input id="newName" type="text" name="name" placeholder="Enter New Staff Name" value="" /></td>';
				html += '<td><input id="newSurname" type="text" name="surname" placeholder="Enter New Staff Surname" value="" /></td>';
				html += '<td><input id="newEmail" type="text" name="email" placeholder="Enter New Staff Email" value="" /></td>';
				html += '<td><input id="addNew" type="button" name="addNewButton" value="Add New" /></td>';
				html += '<td></td>';
				html += '</tr>';
			$.each(data, function(index, data) {
				html += '<tr id="' + data.staffID + '">';
				html += '<td><input id="' + data.staffID + 'id" class="id" type="text" name="staffID" value="' + data.staffID + '" /></td>';
				html += '<td><input id="' + data.staffID + 'name" type="text" name="name" value="' + data.name + '" /></td>';
				html += '<td><input id="' + data.staffID + 'surname" type="text" name="surname" value="' + data.surname + '" /></td>';
				html += '<td><input id="' + data.staffID + 'email" type="text" name="email" value="' + data.email + '" /></td>';
				html += '<td><input id="' + data.staffID + 'update" class="listButton" type="button" name="update" value="Update Changes""/></td>';
				html += '<td><input id="' + data.staffID + 'delete" class="listButton" type="button" name="delete" value="Delete" /></td>';
				html += '</tr>';

				$('#staffTable').html(html);
				
			});
			

		});

	}
	
	//update staff table using filter of search terms from searchBox
	
	$("#searchBox").keyup(function(){
		populateStaff($(this).val());
		
	});

	//listen to clicks on class listButton iether delete or update 

	$(document.body).on('click', '.listButton', function(e) {
		e.preventDefault();
		action = $(this).attr('name');
		id = $(this).parent().parent().attr('id');
		if (action == 'update') {
			updateStaff(id);
		}
		if (action == 'delete') {
			deleteStaff(id);
		}
	});

	//delete staff

	function deleteStaff(id) {
		name = $("#" + id + "name").val();
		var result = confirm('Are you sure you want to delete ' + name + "?");
		if (result == true) {
			$.post("deleteStaff.php", {
				id : id
			}, function(result) {
				if ($.trim(result) == "error") {
					alert("error");
				}

				if ($.trim(result) == "success") {
					populateStaff($("#searchBox").val());
				}

			});
		}

	}

	//update staff

	function updateStaff(oldID) {
		staffID = $("#" + oldID + "id").val();
		name = $("#" + oldID + "name").val();
		surname = $("#" + oldID + "surname").val();
		email = $("#" + oldID + "email").val();
		type = "update";
		//validate staff details before update
		if (validateStaff(oldID, staffID, name, surname, email, type)) {

			//update staff
			$.post("updateStaff.php", {
				oldID : oldID,
				staffID : staffID,
				name : name,
				surname : surname,
				email : email
			}, function(result) {
				if ($.trim(result) == "error") {
					alert("error, please get hold of Geoffry");
				}

				if ($.trim(result) == "success") {
					showPopUp(name+" updated successfully!","adminHome.php","Admin Home","manageStaff.php","Go Back");
					populateStaff($("#searchBox").val());
				}

			});

		}

	}

	//validate staff details for database updates

	function validateStaff(oldID, id, name, surname, email, type) {
		error = false;
		$("#" + id).css("background-image", "none");
		//if type update then check and report error against user id
		if (type == "update") {

			//if id empty
			if (id == "") {
				$("#" + oldID + "id").parent().css("background-image", "url('../admin/img/invalid.png')");
				error = true;
			}

			//if name empty and only characters

			if (name == "" || !name.match(/^[a-zA-Z\s]+$/)) {
				$("#" + oldID + "name").parent().css("background-image", "url('../admin/img/invalid.png')");
				error = true;
			}

			//if surname empty and only characters

			if (surname == "" || !surname.match(/^[a-zA-Z\s]+$/)) {
				$("#" + oldID + "surname").parent().css("color", "red").css("background-image", "url('../admin/img/invalid.png')");
				error = true;
			}

			//if email invalid
			var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
			if (!testEmail.test(email)) {
				$("#" + oldID + "email").parent().css("background-image", "url('../admin/img/invalid.png')");
				error = true;
			}

			//check for any errors reported

			if (error) {
				return false;
			} else {
				return true;
			}

		}
		//if adding new staff check and report error against newStaffID new.. etc
		else if (type == "new") {
			//reset font to black
			$("#addNewStaff").css("color", "black");
			//if id empty
			if (id == "") {
				$("#newStaffID").parent().css("background-image", "url('../admin/img/invalid.png')");
				error = true;
			}

			//if name empty and only characters

			if (name == "" || !name.match(/^[a-zA-Z\s]+$/)) {
				$("#newName").parent().css("background-image", "url('../admin/img/invalid.png')");
				error = true;
			}

			//if surname empty and only characters

			if (surname == "" || !surname.match(/^[a-zA-Z\s]+$/)) {
				$("#newSurname").parent().css("background-image", "url('../admin/img/invalid.png')");
				error = true;
			}

			//if email invalid
			var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
			if (!testEmail.test(email)) {
				$("#newEmail").parent().css("background-image", "url('../admin/img/invalid.png')");
				error = true;
			}

			//check for any errors reported

			if (error) {
				return false;
			} else {
				return true;
			}

		}
	}

	//remove invalid background image when focused on
	$(document.body).on('click', 'td', function(e) {
		$(this).css("background-image", "none");
	});

	//add new staff

	$(document.body).on('click', '#addNew', function(e) {
		e.preventDefault();
		staffID = $("#newStaffID").val();
		name = $("#newName").val();
		surname = $("#newSurname").val();
		email = $("#newEmail").val();
		type = "new";
		//validate inputs
		if (validateStaff(staffID, staffID, name, surname, email, type)) {

			//add new staff to database
			$.post("addStaff.php", {
				staffID : staffID,
				name : name,
				surname : surname,
				email : email
			}, function(result) {
				if ($.trim(result) == "error") {
					alert("error");
				}

				if ($.trim(result) == "success") {
					populateStaff($("#searchBox").val());
					//showPopUp(name+" added successfully!","adminHome.php","Admin Home","manageStaff.php","Go Back");
				}

				if ($.trim(result) == "staffIDExists") {
					alert("Staff ID Already Exists");
				}

				if ($.trim(result) == "emailExists") {
					alert("Email Already In Use");
				}

			});
		}
	});
	
	//takes variables builds and displays popup
	function showPopUp (message, button1Href, button1Msg, button2Href, button2Msg){
		messageHTML = '<h1>'+message+'</h1>';
		button1HTML = '<a href="'+button1Href+'"><p>'+button1Msg+'</p></a>';
		button2HTML = '<a href="'+button2Href+'"><p>'+button2Msg+'</p></a>';
		
		// add message
		$(".popUpHeader").html(messageHTML);
		// add button 1
		$("#button1").html(button1HTML);
		//add button 2
		$("#button2").html(button2HTML);
		//display popup
		$(".popUpWrapper").fadeIn();		
		
	}

	$("#goBack").click(function() {
		$(location).attr('href', "http://localhost/cp/admin/adminHome.php");
	});

});
