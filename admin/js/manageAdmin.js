$(document).ready(function() {

	//first load populateStaff this loads staff into table structure
	populateUsers($("#searchBox").val());

	//hide any popups
	$(".popUpWrapper").hide();

	function populateUsers(filter) {
		//get array of units from database
		//iterate and populate table
		html = '<tr><th>Username</th><th>Email</th></tr>';
		var url = 'grabAdmin.php';
		searchTerms = {
			"searchTerms" : filter
		};
		$.getJSON(url, searchTerms, function(data) {
			html += '<tr id="addNewAdmin">';
			html += '<td><input id="newName" type="text" name="username" placeholder="Admin Username" value="" /></td>';
			html += '<td><input id="newEmail" type="text" name="email" placeholder="Admin Email" value="" /></td>';
			html += '<td><input id="newPassword" type="password" name="password" placeholder="Admin Password" value="" /></td>';
			html += '<td><input class="listButton" id="addNew" type="button" name="addNew" value="Add New" /></td>';
			html += '<td></td>';
			html += '</tr>';
			$.each(data, function(index, data) {
				html += '<tr id="' + data.id + '">';
				html += '<td><input id="' + data.id + 'username" type="text" name="username" value="' + data.username + '" readonly/></td>';
				html += '<td><input id="' + data.id + 'email" type="text" name="email" value="' + data.email + '" readonly/></td>';
				html += '<td><input id="' + data.id + 'delete" class="listButton" type="button" name="delete" value="Delete" /></td>';
				html += '</tr>';

				$('#adminTable').html(html);

			});

		});

	}

	//update staff table using filter of search terms from searchBox

	$("#searchBox").keyup(function() {
		populateUsers($(this).val());

	});

	//listen to clicks on class listButton iether delete or update

	$(document.body).on('click', '.listButton', function(e) {
		e.preventDefault();
		action = $(this).attr('name');
		id = $(this).parent().parent().attr('id');
		if (action == 'delete') {
			deleteUsers(id);
		}
		if (action == 'addNew') {
			addAdmin();
		}
	});

	//delete staff

	function deleteUsers(id) {
		username = $("#" + id + "username").val();
		var result = confirm('Are you sure you want to delete ' + username + "?");
		if (result == true) {
			$.post("deleteAdmin.php", {
				id : id
			}, function(result) {
				if ($.trim(result) == "error") {
					alert("System fault. Please contact administrator");
				}

				if ($.trim(result) == "success") {
					populateUsers($("#searchBox").val());
				}

			});
		}

	}
	
	//add new user
	function addAdmin() {
		username = ($("#newName").val());
		password = ($("#newPassword").val());
		userEmail = ($("#newEmail").val());
		if (validateUsers(username, password, userEmail)) {
			$.post("addAdminUser.php", {
				username : username,
				password : password,
				email : userEmail
			}, function(result) {
				if ($.trim(result) == "userExists") {
					$("#usernameError").text("*Username already exists");
					$("#usernameError").show();
				}

				if ($.trim(result) == "emailExists") {
					$("#emailError").text("*Email already exists");
					$("#emailError").show();
				}

				if ($.trim(result) == "success") {
					//greatSuccess(username); removed as seen as excess pop up
					populateUsers($("#searchBox").val());
				}
				if ($.trim(result) == "error"){
					alert("Database Error, please notify Jeremy, youll find him in his rooms.");
				}

			});

		}

	}
	
	function greatSuccess(usr){
		popUpText = '<ul><li><p>User, '+usr+', hass been succesfully created</p></li><li><p>You can choose to create another user, or go back.</p></li></ul>';
		$("#popUpText").html(popUpText);
		$(".popUpWrapper").fadeIn();
	}

	//validate staff details for database updates

	function validateUsers(username, password, email)  {
		error = false;
		$("#addNewAdmin").css("background-image", "none");

		//reset font to black
		$("#addNewUsers").css("color", "black");
		
		//if name empty and only characters

		if (username == "") {
			$("#newName").parent().css("background-image", "url('../admin/img/invalid.png')");
			error = true;
		}
		
		if (!(password.length >= 8)) {
			$("#newPassword").parent().css("background-image", "url('../admin/img/invalidPassword.png')");
			error = true;
		}
		

		//if email invalid
		var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
		if (!testEmail.test(email) || email == "") {
			$("#newEmail").parent().css("background-image", "url('../admin/img/invalid.png')");
			error = true;
		}

		//check for any errors reported

		if (error) {
			return false;
		} else {
			return true;
		}

	}

	//remove invalid background image when focused on
	$(document.body).on('click', 'td', function(e) {
		$(this).css("background-image", "none");
	});

	//takes variables builds and displays popup
	function showPopUp(message, button1Href, button1Msg, button2Href, button2Msg) {
		messageHTML = '<h1>' + message + '</h1>';
		button1HTML = '<a href="' + button1Href + '"><p>' + button1Msg + '</p></a>';
		button2HTML = '<a href="' + button2Href + '"><p>' + button2Msg + '</p></a>';

		// add message
		$(".popUpHeader").html(messageHTML);
		// add button 1
		$("#button1").html(button1HTML);
		//add button 2
		$("#button2").html(button2HTML);
		//display popup
		$(".popUpWrapper").fadeIn();

	}


	$("#goBack").click(function() {
		$(location).attr('href', "http://localhost/cp/admin/adminHome.php");
	});

});
