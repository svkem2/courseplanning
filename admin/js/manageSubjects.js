$(document).ready(function() {

	//get array of units from database
	var units = new Array();
	function populateUnits() {
		$.getJSON("grabUnits.php", function(data) {

			var i = 0;
			while (i < data.myarray.length) {
				units[i] = data.myarray[i];
				i++;
			}

			$.each(units, function(index, value) {
				unitsList += '<li><p>' + value + '</p></li>';
			});

			unitsList = '<ul>';

			$.each(units, function(index, value) {
				unitsList += '<li><p>' + value + '</p></li>';

			});

			unitsList += '</ul>';

			//add unitList html to #unitList
			$("#unitsList").html(unitsList);

		});
	}

	//populate string with list html containing units in database
	populateUnits();
	
	//button listeners 
	
	$("#goBack").click(function(){
		$(location).attr('href',"http://localhost/cp/admin/adminHome.php");
	});
	

});
