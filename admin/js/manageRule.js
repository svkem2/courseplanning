$(document).ready(function() {

	//first load populateStaff this loads staff into table structure
	populateRules($("#searchBox").val());

	//hide any popups
	$(".popUpWrapper").hide();

	function populateRules(filter) {
		//get array of units from database
		//iterate and populate table
		html = '<tr><th>Rule Name</th><th>Rule Description</th><th>Rule Value</th></tr>';
		var url = 'grabRule.php';
		searchTerms = {
			"searchTerms" : filter
		};
		$.getJSON(url, searchTerms, function(data) {
			html += '<tr id="addNewRule">';
			html += '<td class="new"><input id="newRuleName" type="text" name="ruleName" placeholder="Enter New Rule Name" value="" /></td>';
			html += '<td class="new"><input id="newRuleDescription" type="text" name="ruleDescription" placeholder="Enter New Rule Description" value="" /></td>';
			html += '<td class="new"><input id="newRuleValue" type="text" name="ruleValue" placeholder="Enter New Rule Value" value="" /></td>';
			html += '<td><input id="addNew" type="button" name="addNewButton" value="Add New" /></td>';
			html += '<td></td>';
			html += '</tr>';
			$.each(data, function(index, data) {
				html += '<tr id="' + data.ruleID + '">';
				html += '<td><input id="' + data.ruleID + 'ruleName" type="text" name="ruleName" value="' + data.ruleName + '" readonly/></td>';
				html += '<td><input id="' + data.ruleID + 'ruleDescription" type="text" name="ruleDescription" value="' + data.ruleDescription + '" /></td>';
				html += '<td><input id="' + data.ruleID + 'ruleValue" type="text" name="ruleValue" value="' + data.ruleValue + '" /></td>';
				html += '<td><input id="' + data.ruleID + 'update" class="listButton" type="button" name="update" value="Update Changes""/></td>';
				html += '<td><input id="' + data.ruleID + 'delete" class="listButton" type="button" name="delete" value="Delete" /></td>';
				html += '</tr>';

				$('#rulesTable').html(html);

			});

		});

	}

	//update staff table using filter of search terms from searchBox

	$("#searchBox").keyup(function() {
		populateRules($(this).val());

	});

	//listen to clicks on class listButton either delete or update

	$(document.body).on('click', '.listButton', function(e) {
		e.preventDefault();
		action = $(this).attr('name');
		ruleID = $(this).parent().parent().attr('id');
		if (action == 'update') {
			updateRules(ruleID);
		}
		if (action == 'delete') {
			deleteRules(ruleID);
		}
	});

	//delete staff

	function deleteRules(ruleID) {
		ruleName = $("#" + ruleID + "ruleName").val();
		var result = confirm('Are you sure you want to delete ' + ruleName + "?");
		if (result == true) {
			$.post("deleteRule.php", {
				ruleID : ruleID
			}, function(result) {
				if ($.trim(result) == "error") {
					alert("error");
				}

				if ($.trim(result) == "success") {
					populateRules($("#searchBox").val());
				}

			});
		}

	}

	//update staff

	function updateRules(oldID) {
		ruleName = $("#" + oldID + "ruleName").val();
		ruleDescription = $("#" + oldID + "ruleDescription").val();
		ruleValue = parseInt($("#" + oldID + "ruleValue").val());
		type = "update";
		//validate staff details before update
		if (validateRules(oldID, "", ruleName, ruleDescription, ruleValue, type)) {

			//update staff
			$.post("updateRule.php", {
				ruleName : ruleName,
				ruleDescription : ruleDescription,
				ruleValue : ruleValue,
				oldID : oldID
			}, function(result) {
				if ($.trim(result) == "error") {
					alert("error!");
				}

				if ($.trim(result) == "success") {
					showPopUp(ruleName + " updated successfully!", "adminHome.php", "Admin Home", "manageRule.php", "Go Back");
					populateRules($("#searchBox").val());
				}

			});

		}

	}

	//validate staff details for database updates

	function validateRules(oldID, ruleID, ruleName, ruleDescription, ruleValue, type) {
		error = false;
		$("#" + ruleID).css("background-image", "none");
		$("td").css("background-image","none");
		//if type update then check and report error against user id
		if (type == "update") {

			
			//if name empty and only characters

			if (ruleName == "") {
				$("#" + oldID + "ruleName").parent().css("background-image", "url('../admin/img/invalid.png')");
				error = true;
			}

			if (ruleDescription == "") {
				$("#" + oldID + "ruleDescription").parent().css("background-image", "url('../admin/img/invalid.png')");
				error = true;

			}

			if (!$.isNumeric(ruleValue)) {
				$("#" + oldID + "ruleValue").parent().css("background-image", "url('../admin/img/invalid.png')");
				error = true;
			}

			//check for any errors reported

			if (error) {
				return false;
			} else {
				return true;
			}

		}

		//if adding new staff check and report error against newStaffID new.. etc
		if (type == "new") {
			//reset font to black
			$("#addNewRule").css("color", "black");
			
			//if name empty and only characters

			if (ruleName == "") {
				$("#newRuleName").parent().css("background-image", "url('../admin/img/invalid.png')");
				error = true;
			}

			if (ruleDescription == "") {
				$("#newRuleDescription").parent().css("background-image", "url('../admin/img/invalid.png')");
				error = true;

			}

			if (!$.isNumeric(ruleValue)) {
				$("#newRuleValue").parent().css("background-image", "url('../admin/img/invalid.png')");
				error = true;
			}

			//check for any errors reported

			if (error) {
				return false;
			} else {
				return true;
			}
		}
	}

	//remove invalid background image when focused on
	$(document.body).on('click', 'td', function(e) {
		$(this).css("background-image", "none");
	});
	
	
	
	//add new staff

	$(document.body).on('click', '#addNew', function(e) {
		e.preventDefault();
		ruleName = $("#newRuleName").val();
		ruleDescription = $("#newRuleDescription").val();
		ruleValue = $("#newRuleValue").val();
		type = "new";
		//validate inputs
		if (validateRules("", "", ruleName, ruleDescription, ruleValue, type)) {

			//add new staff to database
			$.post("addRule.php", {
				ruleName : ruleName,
				ruleDescription : ruleDescription,
				ruleValue : ruleValue
			}, function(result) {
				if ($.trim(result) == "error") {
					alert("error");
				}

				if ($.trim(result) == "success") {
					populateRules($("#searchBox").val());
					//showPopUp(ruleName + " added successfully!", "adminHome.php", "Admin Home", "manageRule.php", "Go Back");
				}


				if ($.trim(result) == "nameExists") {
					showPopUp(ruleName + " already exists.", "adminHome.php", "Admin Home", "manageRule.php", "Go Back");
				}

			});
		}
	});

	//takes variables builds and displays popup
	function showPopUp(message, button1Href, button1Msg, button2Href, button2Msg) {
		messageHTML = '<h1>' + message + '</h1>';
		button1HTML = '<a href="' + button1Href + '"><p>' + button1Msg + '</p></a>';
		button2HTML = '<a href="' + button2Href + '"><p>' + button2Msg + '</p></a>';

		// add message
		$(".popUpHeader").html(messageHTML);
		// add button 1
		$("#button1").html(button1HTML);
		//add button 2
		$("#button2").html(button2HTML);
		//display popup
		$(".popUpWrapper").fadeIn();

	}


	$("#goBack").click(function() {
		$(location).attr('href', "http://localhost/cp/admin/adminHome.php");
	});

});
