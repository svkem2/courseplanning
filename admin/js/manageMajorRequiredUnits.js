$(document).ready(function() {

	//first load populateStaff this loads staff into table structure
	populate($("#searchBox").val());

	//hide any popups
	$(".popUpWrapper").hide();

	function populate(filter) {
		//get array of units from database
		//iterate and populate table
		html = '<tr><th>Major Code</th><th>Require Unit Code</th><th>Delete</th></tr>';
		var url = 'grabMajorRequiredUnits.php';
		searchTerms = {
			"searchTerms" : filter
		};
		$.getJSON(url, searchTerms, function(data) {
			html += '<tr id="addNewMajorRequiredUnits">';
			html += '<td>' + getOptions("newMajorCode") + '</td>';
			html += '<td>' + getOptions("newMajorRequiredUnit") + '</td>';
			html += '<td><input id="addNew" class="listButton" type="button" name="new" value="Add New" /></td>';
			html += '<td></td>';
			html += '</tr>';
			$.each(data, function(index, data) {
				html += '<tr id="' + data.majorCode + '" title="' + data.majorRequiredUnit + '" class="' + data.majorCode + data.majorRequiredUnit + '">';
				html += '<td><input id="' + data.majorCode + 'majorCode" class="majorCode" type="text" name="majorCode" value="' + data.majorCode + '" readonly/></td>';
				html += '<td><input id="' + data.majorCode + 'majorRequiredUnit" type="text" name="majorRequiredUnit" value="' + data.majorRequiredUnit + '" readonly/></td>';
				html += '<td><input id="' + data.majorCode + 'delete" class="listButton" type="button" name="delete" value="Delete Required Unit" /></td>';
				html += '</tr>';
				$('#majorRequiredUnitsTable').html(html);
			});
		});

	}

	//create dropdown boxes
	function getOptions(input) {
		text = "";
		phpUrl = "";
		if (input == "newMajorCode") {
			text = "Select Major Code";
			phpUrl = "grabMajors.php";
			response = '<select id="' + input + '">';
			response += "<option value=''>" + text + "</option>";
			$.ajax({
				url : phpUrl,
				dataType : 'json',
				data : {
					searchTerms : ""
				},
				async : false,
				success : function(data) {
					$.each(data, function(index, data) {
						response += '<option value="' + data.majorCode + '">' + data.majorCode + '</option>';
					});
				}
			});
			response += "</select>";

		} else {
			text = "select Unit Code";
			phpUrl = "grabUnitsPlusName.php";
			response = '<select id="' + input + '">';
		response += "<option value=''>" + text + "</option>";
		$.ajax({
			url:phpUrl,
			dataType:'json',
			data: {searchTerms : ""},
			async:false,
			success: function(data){
				$.each(data, function(index,data){
					response += '<option value="' + data.unitCode + '">'+data.unitCode+'</option>';
				});
			}
		});
		response += "</select>";

		}

		return response;
	}

	//update staff table using filter of search terms from searchBox

	$("#searchBox").keyup(function() {
		populate($(this).val());
	});

	//listen to clicks on class listButton iether delete or update

	$(document.body).on('click', '.listButton', function(e) {
		e.preventDefault();
		action = $(this).attr('name');
		thisRow = $(this).parent().parent().attr('class');
		majorCodeValue = $(this).parent().parent().find("[name='majorCode']").val();
		//majorCodeID = $(this).parent().parent().attr('id');
		majorRequiredUnitValue = $(this).parent().parent().find("[name='majorRequiredUnit']").val();
		//majorRequiredUnitID = $(this).parent().parent().attr('title');
		if (action == 'new') {
			addRequiredUnit();
		}
		if (action == 'delete') {
			deleteRequiredUnit(majorCodeValue, majorRequiredUnitValue);
		}
	});

	//delete required unit

	function deleteRequiredUnit(major, unit) {
		var result = confirm('Are you sure you want to remove required unit ' + unit + ' from major ' + major + '?');
		if (result == true) {
			$.post("deleteMajorRequiredUnit.php", {
				major : major,
				unit : unit
			}, function(result) {
				if ($.trim(result) == "error") {
					alert("System error. Please contact administrator.");
					populate($("#searchBox").val());
				}

				if ($.trim(result) == "success") {
					//showPopUp("Unit Removed From Major Requirements", unit + " has been successfully removed from " + major + " as a required unit", "adminHome.php", "Admin Dashboard", "manageMajorRequiredUnits.php", "Go Back");
					populate($("#searchBox").val());
				}

			});
		}

	}

	//update required unit

	function addRequiredUnit() {
		
		major = $("#newMajorCode").val();
		unit = $("#newMajorRequiredUnit").val();
		if (validateStaffAllocation(major, unit)) {
			$.post("updateMajorRequiredUnit.php", {
				majorValue : major,
				unitValue : unit
			}, function(result) {
				if ($.trim(result) == "error") {
					alert("System fault. Please contact administrator");
				}
				
				if($.trim(result) == "requirementExists"){
					showPopUp("Required Unit Already Exists", unit + " has already been allocated to " + major, "adminHome.php", "Dashoboard Home", "manageMajorRequiredUnits.php", "Go Back");
					populate($("#searchBox").val());
				}

				if ($.trim(result) == "success") {
					//showPopUp("Update Success", unit + " has been successfully allocated to " + major, "adminHome.php", "Dashoboard Home", "manageMajorRequiredUnits.php", "Go Back");
					populate($("#searchBox").val());
				}

			});

		}
	}

	//validate details for database updates

	function validateStaffAllocation(majorValue, unitValue) {
		error = false;
		//check majorValue not empty
		if (majorValue == "") {
			$("#newMajorCode").parent().css("background-image", "url('../admin/img/invalid.png')");
			error = true;
		}

		//if unit code empty
		if (unitValue == "") {
			$("#newMajorRequiredUnit").parent().css("background-image", "url('../admin/img/invalid.png')");
			error = true;

		}

		if (error) {
			return false;
		} else {
			return true;
		}

	}

	//remove invalid background image when focused on
	$(document.body).on('click', 'td', function(e) {
		$(this).css("background-image", "none");
	});

	//add new staff

	$(document.body).on('click', '#addkkkNew', function(e) {
		e.preventDefault();
		major = $("#majorCode").val();
		unit = $("#newMajorRequiredUnit").val();
		
		alert(major+unit);
		
		type = "new";
		//validate inputs
		if (validateUnit(unitCode, unitCode, unitDescription, type)) {

			//add new staff to database
			$.post("addUnit.php", {
				unitCode : unitCode,
				unitDescription : unitDescription
			}, function(result) {
				if ($.trim(result) == "error") {
					alert("error");
				}

				if ($.trim(result) == "success") {
					populateUnits("");
					showPopUp(unitCode + " added successfully!", "adminHome.php", "Admin Home", "manageUnits.php", "Go Back");
				}

				if ($.trim(result) == "unitExists") {
					showPopUp(unitCode + " already exists!", "adminHome.php", "Admin Home", "manageUnits.php", "Go Back");
				}

				if ($.trim(result) == "descriptionExists") {
					showPopUp(unitDescription + " already exists!", "adminHome.php", "Admin Home", "manageUnits.php", "Go Back");
				}

				if ($.trim(result) == "error1") {
					showPopUp(unitDescription + " tried to insert!", "adminHome.php", "Admin Home", "manageUnits.php", "Go Back");
				}

			});
		}
	});

	//takes variables builds and displays popup
	function showPopUp(header, message, button1Href, button1Msg, button2Href, button2Msg) {
		headerHTML = '<h1>' + header + '</h1>';
		messageHTML = message;
		button1HTML = '<a href="' + button1Href + '"><p>' + button1Msg + '</p></a>';
		button2HTML = '<a href="' + button2Href + '"><p>' + button2Msg + '</p></a>';

		// add message
		$(".popUpHeader").html(headerHTML);

		$('#popUpText').html(messageHTML).css('padding', '20px 20px 0px 20px');
		// add button 1
		$("#button1").html(button1HTML);
		//add button 2
		$("#button2").html(button2HTML);
		//display popup
		$(".popUpWrapper").fadeIn();

	}


	$("#goBack").click(function() {
		$(location).attr('href', "http://localhost/cp/admin/adminHome.php");
	});

});
