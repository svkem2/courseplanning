$(document).ready(function() {

	//first load populateStaff this loads staff into table structure
	populateAllocations($("#searchBox").val());

	//hide any popups
	$(".popUpWrapper").hide();

	function generateStaffOptions(selectedStaffID) {
		response = "<option value=''>Select Staff Member!</option>";
		$.ajax({
			url : 'grabStaff.php',
			data : {
				searchTerms : ""
			},
			dataType : 'json',
			async : false,
			success : function(data) {
				$.each(data, function(index, data) {
					response += "<option " + ((data.staffID == selectedStaffID) ? "selected" : "") + " value='" + data.staffID + "'>" + (data.name + " " + data.surname ) + "</option>";
				});
			}
		});

		return response;
	}

	function populateAllocations(filter) {
		//get array of units from database
		//iterate and populate table
		html = '<tr><th>Unit Code</th><th>Unit Name</th><th>Staff Member</th><th>Update</th><th>Clear</th></tr>';
		var url = 'grabStaffAllocations.php';
		searchTerms = {
			"searchTerms" : filter
		};
		$.getJSON(url, searchTerms, function(data) {
			html += '<tr id="newStaffAllocation">';
			html += '<td>' + getOptions("newUnitCode") + '</td>';
			html += '<td>' + getOptions("newStaffID") + '</td>';
			html += '<td><input id="addNew" class="listButton" type="button" name="new" value="Add New" /></td>';
			html += '<td></td>';
			html += '</tr>';
			$.each(data, function(index, data) {
				html += '<tr id="' + data.allocID + '">';
				html += '<td><input id="' + data.allocID + 'unitCode" class="unitCode" type="text" name="unitCode" value="' + data.unitCode + '" readonly/></td>';
				html += '<td><input id="' + data.allocID + 'unitDescription" type="text" name="unitDescription" value="' + data.unitDescription + '" readonly/></td>';
				html += '<td><select id="' + data.allocID + 'staffID">' + generateStaffOptions(data.staffID) + '</select></td>';
				html += '<td><input id="' + data.allocID + 'update" class="listButton" type="button" name="update" value="Update Changes""/></td>';
				html += '<td><input id="' + data.allocID + 'delete" class="listButton" type="button" name="delete" value="Clear Allocation" /></td>';
				html += '</tr>';
				$('#staffAllocationsTable').html(html);
			});
		});

	}
	
	//create dropdown boxes
	function getOptions(input) {
		text = "";
		phpUrl = "";
		if (input == "newUnitCode") {
			text = "Select Unit Code";
			phpUrl = "grabUnitsPlusName.php";
			response = '<select id="' + input + '">';
			response += "<option value=''>" + text + "</option>";
			$.ajax({
				url : phpUrl,
				dataType : 'json',
				data : {
					searchTerms : ""
				},
				async : false,
				success : function(data) {
					$.each(data, function(index, data) {
						response += '<option value="' + data.unitCode + '">' + data.unitCode + '</option>';
					});
				}
			});
			response += "</select>";

		} else {
			text = "select Staff";
			phpUrl = "grabStaff.php";
			response = '<select id="' + input + '">';
		response += "<option value=''>" + text + "</option>";
		$.ajax({
			url:phpUrl,
			dataType:'json',
			data: {searchTerms : ""},
			async:false,
			success: function(data){
				$.each(data, function(index,data){
					response += '<option value="' + data.staffID + '">'+data.name+ " " + data.surname+'</option>';
				});
			}
		});
		response += "</select>";

		}

		return response;
	}


	//update staff table using filter of search terms from searchBox

	$("#searchBox").keyup(function() {
		populateAllocations($(this).val());
	});

	//listen to clicks on class listButton iether delete or update

	$(document.body).on('click', '.listButton', function(e) {
		e.preventDefault();
		action = $(this).attr('name');
		allocID = $(this).parent().parent().attr('id');
		if (action == 'update') {
			updateStaffAllocation(allocID);
		}
		if (action == 'delete') {
			deleteStaffAllocation(allocID);
		}
	});

	//delete staff

	function deleteStaffAllocation(allocID) {
		name = $("#" + allocID + "staffID").find('option:selected').text();
		unit = $("#" + allocID + "unitCode").val() + " - " + $("#" + allocID + "unitDescription").val();

		var result = confirm('Are you sure you want to remove  ' + name + ' from ' + unit + '?');
		if (result == true) {
			$.post("deleteStaffAllocation.php", {
				allocID : allocID
			}, function(result) {
				if ($.trim(result) == "error") {
					alert("error");
					populateAllocations($("#searchBox").val());
				}

				if ($.trim(result) == "success") {
					//showPopUp("Clear Success", name + " has been successfully removed from " + unit + "!", "manageStaff.php", "Manage Staff", "manageStaffAllocations.php", "Go Back");
					populateAllocations($("#searchBox").val());
				}

			});
		}

	}

	//update staff

	function updateStaffAllocation(allocID) {
		staffID = $("#" + allocID + "staffID").find('option:selected').val().trim();
		name = $("#" + allocID + "staffID").find('option:selected').text();
		unit = $("#" + allocID + "unitCode").val() + " - " + $("#" + allocID + "unitDescription").val();

		if (validateStaffAllocation(allocID, staffID, "update")) {
			$.post("updateStaffAllocation.php", {
				allocID : allocID,
				staffID : staffID
			}, function(result) {
				if ($.trim(result) == "error") {
					alert("error, please get hold of Geoffry");
				}

				if ($.trim(result) == "success") {
					showPopUp("Update Success", name + " has been successfully allocation to " + unit + "!", "manageStaff.php", "Manage Staff", "manageStaffAllocations.php", "Go Back");
					populateAllocations($("#searchBox").val());
				}

			});

		}
	}

	//validate staff details for database updates

	function validateStaffAllocation(allocID, staffID, type) {
		error = false;
		$("#" + allocID).css("background-image", "none");

		//if type update then check and report error against user id
		if (type == "update") {

			//if staff id empty
			if (staffID.length == 0) {
				$("#" + allocID + "staffID").css("background-color", "red");
				error = true;
			}

			if (error) {
				return false;
			} else {
				return true;
			}

		}
		//if adding new staff check and report error against newStaffID new.. etc
		else if (type == "new") {
			//reset font to black
			$("#addNewUnit").css("color", "black");
			//if id empty
			if (allocID == "") {
				$("#newUnitCode").parent().css("background-image", "url('../admin/img/invalid.png')");
				error = true;
			}

			//if name empty and only characters

			if (staffID == "") {
				$("#newStaffID").parent().css("background-image", "url('../admin/img/invalid.png')");
				error = true;
			}

			//check for any errors reported

			if (error) {
				return false;
			} else {
				return true;
			}

		}
	}

	//remove invalid background image when focused on
	$(document.body).on('click', 'td', function(e) {
		$(this).css("background-image", "none");
	});

	//add new staff

	$(document.body).on('click', '#addNew', function(e) {
		e.preventDefault();
		unitCode = $("#newUnitCode").val();
		staffID = $("#newStaffID").val();
		type = "new";
		//validate inputs
		if (validateStaffAllocation(unitCode, staffID, type)) {

			//add new staff to database
			$.post("addStaffAllocation.php", {
				unitCode : unitCode,
				staffID : staffID
			}, function(result) {
				if ($.trim(result) == "error") {
					alert("System Fault. Please contact the system administrator.");
				}

				if ($.trim(result) == "success") {
					populateAllocations($("#searchBox").val());
					//showPopUp("Staff allocation added successfully!","", "adminHome.php", "Admin Home", "manageStaffAllocations.php", "Go Back");
				}

				if ($.trim(result) == "allocationExists") {
					showPopUp("Staff allocation already exists","", "adminHome.php", "Admin Home", "manageStaffAllocations.php", "Go Back");
				}

				
			});
		}
	});

	//takes variables builds and displays popup
	function showPopUp(header, message, button1Href, button1Msg, button2Href, button2Msg) {
		headerHTML = '<h1>' + header + '</h1>';
		messageHTML = message;
		button1HTML = '<a href="' + button1Href + '"><p>' + button1Msg + '</p></a>';
		button2HTML = '<a href="' + button2Href + '"><p>' + button2Msg + '</p></a>';

		// add message
		$(".popUpHeader").html(headerHTML);

		$('#popUpText').html(messageHTML).css('padding', '20px 20px 0px 20px');
		// add button 1
		$("#button1").html(button1HTML);
		//add button 2
		$("#button2").html(button2HTML);
		//display popup
		$(".popUpWrapper").fadeIn();

	}


	$("#goBack").click(function() {
		$(location).attr('href', "http://localhost/cp/admin/adminHome.php");
	});

});
