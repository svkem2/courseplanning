<?php
require ("common.php");
if (empty($_SESSION['user'])) {
	header("Location: login.php");
	die("Redirecting to login.php");
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN"
"http://www.w3.org/TR/html4/strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Course Planning Administration</title>
		<meta name="author" content="stu" />
		<!-- CSS
		============================================================================= -->
		<link href="../css/basic.css" rel="stylesheet" type="text/css" />

		<link href="../admin/css/backupDatabase.css" rel="stylesheet" type="text/css" />
		<!-- SCRIPTS
		============================================================================= -->
		<script src="js/jquery-1.10.2.min.js"></script>
		<script src="js/backup.js"></script>
	</head>
	<body>
		<!-- javascript requirment popup -->
		<div class="popUpWrapper hidden">
			<div class="popUp rounded">
				<div class="popUpHeader">
					<h1>Database Backed Up To Server</h1>
				</div>
				<div id="popUpText"></div>
				<div class="buttonArea">
					<div id="goHome" class="button buttonFloatLeft">
						<a href="adminHome.php">
						<p>
							Admin Dashboard
						</p></a>
					</div>
					
				</div>
			</div>
		</div>
		<div class="contentBox rounded dropShadow">
			<div id="header"></div>
			<div class="pageTitle">
				<p>
					<a href="adminHome.php">Admin Dashboard </a><span>> </span>Database Backup <span id="logout"><a href="logout.php">Logout</a></span>
				</p>
			</div>
			<div id="backupContent">
				<h1>Backup Database to Server</h1>
			</div>
			<div class="buttonArea">
				<div id="goBack" class="button buttonFloatLeft">
					<a href="adminHome.php">
					<p>
						Go Back
					</p></a>
				</div>
				<div id="backup" class="button buttonFloatRight">
					<a href="#">
					<p>
						Backup Database
					</p></a>
				</div>

			</div>


		</div><!-- end contentBox -->

	</body>
</html>

