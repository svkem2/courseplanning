<?php

require ("loginTest.php");

//check if staff id exists
$query = "
SELECT
*
FROM units
WHERE
unitCode = :unitCode
";

$query_params = array(':unitCode' => $_POST['unitCode']);

try {

	$stmt = $db -> prepare($query);
	$result = $stmt -> execute($query_params);
} catch (PDOException $ex) {

	die("error");
}

$row = $stmt -> fetch();

if ($row) {
	die("unitExists");
}

$query = "
SELECT
*
FROM units
WHERE
unitDescription = :unitDescription
";

$query_params = array(':unitDescription' => $_POST['unitDescription']);

try {
	$stmt = $db -> prepare($query);
	$result = $stmt -> execute($query_params);
} catch (PDOException $ex) {
	die("error");
}

$row = $stmt -> fetch();

if ($row) {
	die("descriptionExists");
}


$query = "
INSERT INTO units (
unitCode,
unitDescription
) VALUES (
:unitCode,
:unitDescription
)
";



$query_params = array(':unitCode' => $_POST['unitCode'], ':unitDescription' => $_POST['unitDescription']);

try { 

	$stmt = $db -> prepare($query);
	$result = $stmt -> execute($query_params);
	die("success");
} catch (PDOException $ex) {

	die("error"+$ex);
}

?>
