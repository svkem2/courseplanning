<?php

require ("common.php");

$query = "
SELECT
1
FROM users
WHERE
username = :username
";

$query_params = array(':username' => $_POST['username']);

try {

	$stmt = $db -> prepare($query);
	$result = $stmt -> execute($query_params);
} catch (PDOException $ex) {

	die("error");
}

$row = $stmt -> fetch();

if ($row) {
	die("userExists");
}

$query = "
SELECT
1
FROM users
WHERE
email = :email
";

$query_params = array(':email' => $_POST['email']);

try {
	$stmt = $db -> prepare($query);
	$result = $stmt -> execute($query_params);
} catch (PDOException $ex) {
	die("error");
}

$row = $stmt -> fetch();

if ($row) {
	die("emailExists");
}

$query = "
INSERT INTO users (
username,
password,
salt,
email
) VALUES (
:username,
:password,
:salt,
:email
)
";

$salt = dechex(mt_rand(0, 2147483647)) . dechex(mt_rand(0, 2147483647));

$password = hash('sha256', $_POST['password'] . $salt);

for ($round = 0; $round < 65536; $round++) {
	$password = hash('sha256', $password . $salt);
}

$query_params = array(':username' => $_POST['username'], ':password' => $password, ':salt' => $salt, ':email' => $_POST['email']);

try {

	$stmt = $db -> prepare($query);
	$result = $stmt -> execute($query_params);
	die("success");

} catch (PDOException $ex) {

	die("error");
}
?>
