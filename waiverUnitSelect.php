<?php session_start(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN"
"http://www.w3.org/TR/html4/strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Select Units, Course Prerequisite Waiver</title>
		<meta name="author" content="stu" />
		<!-- CSS
		============================================================================= -->
		<link href="css/basic.css" rel="stylesheet" type="text/css" />
		<!-- SCRIPTS
		============================================================================= -->
		<script src="js/jquery-1.10.2.min.js"></script>
		<script src="js/unitSelection.js"></script>

	</head>
	<body>
		<!-- Connect to database -->
		<?php
		$conn = mysql_connect("localhost", "root", "svkem2") or die('I cannot connect to the database because: ' . mysql_error());
		$db = mysql_select_db("test", $conn);
		$query = "SELECT unitCode FROM units";
		$result = mysql_query($query);
		?>
		

		<div class="contentBox rounded dropShadow">
			<div id="header"></div>
			<div class="pageTitle">
				<p>
					<a href="index.html">Home </a><span>> </span>Prerequisite Waiver Request Form
				</p>
			</div>
			<div class="infoPanel rounded">
				<ul>
					<li>
						<p>
							First select the number of units you wish to apply for prerequisite waivers.
						</p>
					</li>
					<li>
						<p>
							Then select the unit codes from the drop-down boxes provided.
						</p>
					</li>
					<li>
						<p>
							When you have selected your units click the 'Continue' to proceed.
						</p>
					</li>
				</ul>
			</div>
			<table class="layoutTable">
				<tr>
					<td><h1>How many units are you applying for?</h1></td>
					<td>
					<select id="numChosen" class="selectPanel floatRight40">
						<option value="0" selected>Choose number of subjects</option>
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>

					</select></td>
				</tr>
			</table>
			<table id="unitSelection" class="layoutTable">
				<tr id="unit1Row" class="hidden">
					<td><h1>Select Unit by Code</h1></td>
					<td>
					<select id="unit1" class="selectPanel floatRight40">
						<!-- html for unit1 goes here -->
					</select></td>
				</tr>
				<tr id="unit2Row" class="hidden">
					<td></td>
					<td>
					<select id="unit2" class="selectPanel floatRight40">
						<!-- html for unit2 goes here -->
					</select></td>
				</tr>
				<tr id="unit3Row" class="hidden">
					<td></td>
					<td>
					<select id="unit3" class="selectPanel floatRight40">
						<!-- html for unit1 goes here -->
					</select></td>
				</tr>

			</table>
			<br />
			<div class="buttonArea">
				<div id="proceed" class="button buttonFloatRight">
					<p>
						Continue
					</p>
				</div>
				<div id="goBackButton" class="button buttonFloatLeft">
					<a href="index.html">
					<p>
						Go Back
					</p></a>
				</div>
			</div>
		</div><!-- end contentBox -->
		
		<?php
		mysql_free_result($result);
		mysql_close($conn);
		?>

	</body>
</html>

