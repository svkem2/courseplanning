<?php
require_once (dirname(__FILE__) . '/mPDF/mpdf.php');

$addHTML = $_POST['planner'];
$fileName = '/var/www/html/tmp/' . $_POST['file'] . '.pdf';
//$tmpFile = fopen("tmp.pdf", "w");
convert($addHTML, $fileName);

//send email
require_once (dirname(__FILE__) . '/mailer/class.phpmailer.php');
$email = new PHPMailer();
$email -> From = 'team8@student.monash.edu';
$email -> FromName = 'Team 8';
$email -> Subject = 'Course Planning Checklist';
$email -> Body = '<p>To Whom It May Concern,</p><p>Please find the attached course planning checklist</p><br /><p>Thank you</p><p>Kind Regards, Team 8</p>';
$email -> AddAddress('svkem2@student.monash.edu');
//$email -> addCC('ckap5@student.monash.edu');

$email -> AddAttachment($fileName, 'CourseChecklist.pdf');
$email -> isHTML(true);
if (!$email -> send()) {
	echo 'emailError';
	//delete file
	unlink($fileName);

} else {
	echo 'success';
	//delete file
	//delete file
	unlink($fileName);
}

function convert($contents, $file) {
	try {

		$mpdf = new mPDF('c', 'A4', '');

		$mpdf -> SetDisplayMode('fullpage');

		//$mpdf -> list_indent_first_level = 0;
		// 1 or 0 - whether to indent the first level of a list

		// LOAD a stylesheet
		$stylesheet = file_get_contents('css/planningForm.css');
		$mpdf -> WriteHTML($stylesheet, 1);
		// The parameter 1 tells that this is css/style only and no body/html/text

		$mpdf -> WriteHTML($contents);

		$mpdf -> Output($file, 'F');

		echo "success";
	} catch(HTML2PDF_exception $e) {
		echo "pdferror";
	}

}
?>